# Logging with Promtail, Loki & Grafana

This document will describe the logging of the JES and its executed apps with Promtail/Loki/Grafana. Depending on the container client, there is a different implementation of promtail:
* **Docker**: JES app, JES database and App-container logs are forwarded through a promtail container
* **Kubernetes**: JES app and JES database logs are forwarded through a promtail container, App Pod logs are forwarded through promtail pods which are created by a daemonset. 

Hence, for both cases, we need a promtail container forwarding docker container logs to Loki. Just for the latter case, when we use the JES with a kubernetes cluster, we need to deploy a daemonset which creates promtail pods on each cluster node to forward app pod logs.

## Container log forwarding

The `docker-compose.yml` includes a configuration for formatting the logs of the JES and its database
so that they can be sent to Loki and queries in Grafana. Promtail is currently not started automatically together
with the JES, but via the separate `docker-compose.yml`.

**Note:** Promtail will only monitor Docker logs, and only if those are formatted and labeled properly:
* JES app and database are running in docker container, independently of the container client (i.e. docker or kubernetes). Apps are either run in containers for docker or in pods for kubernetes, hence, only if using docker the promtail container will forward the App logs. For kubernetes, refer to [this part](#pod-log-forwarding-with-a-daemonset).
* If the JES is started via `uvicorn`, the logs will not be sent to the Logging Service.

To set up the Promtail Service:
* create another `.env` file in the `promtail/` directory, defining the following variables:
  * `LOKI_URL`: the URL incl. port of the Loki service (or corresponding redirect); for EMPAIA, this is currently <https://empaia.dai-labor.de/logging-loki>
  * `TOPIC_LABEL`: which logs to send, typically `empaia`, must be the same as for JES itself (see `JES_TOPIC_LABEL` below) 
  * `PROMTAIL_PWD`: the password for `user1` configured in the Logging Service

**Note:** Promtail will monitor all logs of other Docker containers running on the current machine (if they have the 
`empaia` tag). That means, if you are using a remote `DOCKER_HOST`, you will have to start another instance of the
JES Promtail Docker container on that host in order to also capture the logs of apps running on that remote machine.

As with all services, the `log_label`, which is defined in the top-level `.env`, is used to identify the service the log stems from.
We recommend a format of `service-location-deployment` for this label, e.g. `JES-TUB-NIGHTLY`. The default here is
`jes-unknown` to indicate that the property has not been set properly. Of course, for local testing or for CI, this label
is irrelevant, since those should not use Promtail in the first place.

Compared with the basic Promtail configuration, the JES adds two additional elements to each log line: the `job_id` and
`service_url` (the URL of the App-Service the Job is communicating with). For the JES itself those are empty, but since
they are needed for the individual Job Executions, and both the JES and the App logs are parsed by the same Promtail,
the JES has to define those, too.
For the apps, the `get_log_config` function in the `jobexecutor.py` determines, how the app logs are tagged. 

**Note:** In order not to blow up the index, the `job_id` and `service_url` attributes are not "Labels". Thus, you can not
just select the Job-ID as a Label in the Grafana-UI. Instead, you have to manually create a Query. For this, go to "Explore"
and instead of expanding the "Log Browser", enter a query directly and press `Shift+Enter` to execute it, for instance:
`{log_label="jes-local-test"} | unpack | job_id="b6f523c0-cdaa-47c0-b060-5d7754a4ceb0"`. This will search all logs that
are labeled with `jes-local-test` (of course this has to be adapted to the label used for your JES instance) for the given
`job_id`. The same also works with `service_url`.

Also refer to <https://gitlab.cc-asp.fraunhofer.de/empaia/platform/common-services/logging-service> for more
and general information on the EMPAIA Logging Service.


## Pod log forwarding with a DaemonSet

As described above, if running the JES with kubernetes support (hence, Apps are executed in pods) the logs of the Apps are not forwarded as done for the JES app and database. Hence, we need to deploy a daemonset on the kubernetes cluster which deploys promtail pods on each node with associated reading rights. The grafana template for this can be found [here](https://grafana.com/docs/loki/latest/clients/promtail/installation/#daemonset-recommended). In the `kubernetes` directory, you can find a JES-specific template which requires minimal adaptation. Each line with a comment ("#") requires adaptation.

### Advanced insights
The daemonset.yaml does not soly deploy a daemonset but all connected and required resources. These are:
* ConfigMap: config-"template" of the pods which are created by the daemonset
* Secrets: information on username and password
* ServiceAccount: daemonset specific account
* ClusterRole: rights for the daemonset (like reading pods)
* ClusterRoleBinding: binds the ClusterRole with the ServiceAccount

For more information on them, refer to the kubernetes website. 