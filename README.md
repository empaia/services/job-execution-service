# job-execution-service

Implementation of the EMPAIA Job-Execution-Service using FastAPI.

This service will not run in the Medical Data Service, and one instance of this service may
be responsible for executing jobs by different institutions. Therefore, the service is
designed in such as way that it does not have to query the Medical Data Service but gets all
the information it needs in a job execution request (see below). It will then start the App
(which will communicate with the responsible Medical Data Service via the App Service) and
provide an endpoint to querying the current status of the Job.


## Routes

### `POST /v1/executions`

Schedules the given Job for execution.

* Input: `PostExecution` including the App to start, the parameters needed by the app, and optional timeout
  (in seconds, or `-1` for no timeout).
    
    ``` json
    {
      "job_id": ID,
      "app_id": ID,
      "access_token": string,
      "app_service_url": URL,
      "timeout": int,
      "mode": string
    }
    ```

* Output: full `Execution` (see below), or HTTP Error in case of problems

### `GET /v1/executions/{job_id}`

Retrieve the status of the given Job from the Job Execution Service's database.

* Input: The ID of the Job

* Output: `Execution` of the Job, or None if no matching Job is found. The error message is optional
  and depends on the job's status. For the GET route, the access token is blanked out.
    
    ``` json
    {
      "job_id": ID,
      "app_id": ID,
      "access_token": string,
      "app_service_url": URL,
      "timeout": int,
      "mode": string,
      "status": string,
      "exit_code": int,
      "message": string,
      "created_at": int,
      "started_at": int,
      "ended_at": int,
      "organization_id": str
    }
    ```

### `PUT /v1/executions/{job_id}/stop`

Cancel a scheduled job or abort an already running execution. In both cases, the Status will be set to `STOPPED`
with a corresponding status message.

* Input: The ID of the Job

* Output: `bool` indicating whether the execution was actually stopped (True) or already completed (false)


### Execution Status

The Execution's `status` is based on the `JobStatus` and is one of the following:

* `SCHEDULED`: scheduled for execution
* `RUNNING`: currently running
* `TERMINATED`: completed by itself, with any return code
* `STOPPED`: stopped or cancelled by user
* `TIMEOUT`: not completed before timeout set in exec. request
* `ERROR`: indicating error with job executor, i.e., outside of app

The statuses `ASSEMBLY`, `READY`, and `INCOMPLETE` are not relevant for the JES.

### Security: `organization_id`

Each request requires an `organization_id` to be passed as a Header. The same `organization_id`
has to be used for accessing the job's Execution status as was used for creating the Execution.

If the JES requires authentication, then this `organization_id` has to match one of the organizations
in the given auth token (JWT).

If the JES does not require authentication, then any value can be passed as the `organization_id`, but it still
has to be the same for `POST`ing an Execution and for `GET`ing its status. Also, the `organization_id` is used
to determine which Apps may be used (i.e. only those purchased by that organization).

## Job Priority & Scheduling

### Execution Modes and Worker Queues

Jobs can be executed in different modes. Depending on the mode, the app may have different requirements and the job is sorted into a different queue, to be handled by different workers. Valid Job Modes are: `STANDALONE` `PREPROCESSING`, `POSTPROCESSING`, and `REPORT`. Note, however, that at the moment the JES only has Queues for `STANDALONE` and `PREPROCESSING`; `POSTPROCESSING` jobs will be sorted into the `STANDALONE` queue, since they behave similarly, while `REPORT` jobs are not supported (they are not to be executed on JES at all).

If the requirements for a given app can not be determined, the job will be sorted into the "best" queue for the given Mode (e.g. into `STANDALONE-GPU`, if that queue has any workers, else in `STANDALONE-CPU` or, if that too should not have any workers, in `STANDALONE-PROXY` for a `STANDALONE` job).

The different queues and their workers are specified in different Environment Variables (see below). Each worker can run on a different Docker host, use GPU or not, and have access to some or all GPU devices. Specifications for different worker (groups) are separated with `|` (pipe), and the format for a single worker or group is `host;spec`, where `spec` is one of either:

* for GPU Workers, a comma-separated list of GPU Device-ID, e.g. `1,2,3`, or empty for "all GPUs"
* for non-GPU Workers, an `x` followed by the number of workers, e.g. `x4`

Note that while one `spec` can create several non-GPU workers using the `x` notation, a spec for a GPU worker will always create exactly one worker that has access to the specified GPU devices, and not one worker for each of those devices! For multiple GPU workers, repeat the `host;spec` string an according number of times, e.g. for the same host but different GPU devices.

The `host` part can also be empty, meaning the workers will run on the default host (e.g. local Docker).

### App Requirements

The requirements for different Apps in different execution modes are still work-in-progress. At the moment, apps are categorized as one of three types:

* `GPU`: The app requires GPU support.
* `CPU`: The app does not require GPU support, but produces significant CPU load.
* `PROXY`: The app acts mainly as a proxy to some external services, thus requiring few local resources.

In future releases, the App Requirements will by provided by the Marketplace-Service; until then, they are baked directly into the JES Container Image:

* Create a JSON file containing the requirements for all relevant apps in all relevant modes. See `app_requirements.json` in this repository as a template.
* The file contains a JSON object with three levels: On the top level are the namespaces of the apps (case-sensitive), below that all relevant modes that app can be executed in (see "Execution Modes" above, in upper-case), and below that the actual app-requirements, which currently only have one value, the `"type"` (see list above),
* Adapt the Dockerfile or create a new Dockerfile extending `FROM` the JES Dockerfile to `COPY` the file into the image and adapt the `JES_APP_REQUIREMENTS_FILE` environment variable accordingly.

## Data Persistence

The Job Execution Service uses a Mongo DB for persisting the Executions between sessions. This is particularly
important for picking up scheduled or already running Jobs when JES is restarted. By default, those Executions
are stored internally in the `mongo` Docker container, and will be persisted if the container is stopped and
restarted, but are lost if the container is removed (including when doing `docker compose down`).

To fix this, it is advised to mount the `/data/db` directory from the container in a Docker volume. See the 
`docker-compose.yml` for an example.


## Logging

### Viewing Job Container Logs

Depending on the container client, viewing the logs differs:
* **Docker**: App Containers are started using the `job_id` as the container's name, so the logs of the containers can be viewed with `docker logs <job_id>`.
* **Kubernetes**: App pods are started using the `job_id` with prefix `"jes-job"` and an additional unique string as the container's name. So best practice is to first get all pods `kubectl get pods -A -o wide` and then the logs of the desired pod `kubectl logs pod jes-job-<job_id>-<identifier>`.

### Logging with Promtail, Loki & Grafana

Promtail is currently not started automatically together
with the JES but via a separate `docker-compose.yml` in the `promtail/` directory and depending on the container client requires a daemonset deployment. For a detailed documentation refer to the [promtail-targeted README](promtail/README.md).

## Environment Variables

Any environmental variable listed below can be set in a `.env` file in the project directory. This file is ignored by Git. Some Variables are **required**; if optional variables are not set, the default will be applied (defined in `settings.py`). An example `.env` file is provided in `sample.env` file.

Note: The `docker-compose.yml` is set up such that environment variables that are not set at all are also not set within the container, i.e. in that case the default from `settings.py` will be used (if any). But if a variable is listed in `.env` but does not have a value, its value will be `""`, which may yield a TypeError or otherwise unwanted behavior.

### API
* `JES_ROOT_PATH` set if server runs behind a proxy, else openapi `/docs` will not work.
* `JES_DISABLE_OPENAPI` If you want to disable openapi `/docs` in productive mode.


### Database
* `JES_DATABASE_URL` (**required**) is the URL where to find the MongoDB, e.g., in another Docker image.
* `JES_DATABASE_NAME` the name of the database table to use, only needed if, e.g., two instances are running on the
  same system, using the same DB; defaults to `job-execution-service-db`.


#### Job Runner Configuration
* `JES_TIME_TO_CLEAN_UP_JOBS_AFTER_COMPLETION` defines the time in seconds until app containers/pods are removed from disk (minimum 60 seconds). If set to `-1` containers/pods are not removed.

* `JES_WORKER_STANDALONE_GPU`: Workers for Standalone Jobs of type "GPU".
* `JES_WORKER_STANDALONE_CPU`: Workers for Standalone Jobs of type "CPU".
* `JES_WORKER_STANDALONE_PROXY`: Workers for Standalone Jobs of type "Proxy".
* `JES_WORKER_PREPROCESSING_GPU`: Workers for Preprocessing Jobs of type "GPU".
* `JES_WORKER_PREPROCESSING_CPU`: Workers for Preprocessing Jobs of type "CPU".
* `JES_WORKER_PREPROCESSING_PROXY`: Workers for Preprocessing Jobs of type "Proxy".

### Docker
* `JES_DOCKER_HOST` specifies the Docker hosts available for running the actual apps; default is `None`, or `""`", i.e. the same machine the JES itself is running on (or the same Docker, if JES itself is running in Docker). Multiple hosts can be passed, separated by `;` (trailing `;` means last option is empty string, i.e. local host). Which of those hosts is used is determined in the worker-specifications (see above).
* `JES_DOCKER_NETWORK` Docker network to connect apps on startup. Relevant if the app-service runs on the same host, but in another Docker bridged network.
* `JES_DOCKER_EXTRA_HOSTS` defines a `,` separated list of extra host mappings for name resolution in job containers; defaults to `None`
* `JES_DOCKER_EXTRA_ENV_VARS` can be used to define additional environment variables to be passed to the App containers; expecting a list of strings in `key=value` format, e.g. `["foo=bar","x=5"]`, values without `=` are ignored.
* `JES_DOCKER_ALWAYS_PULL` decides whether to always try to re-pull the app image from registry, even if the image and tag already exist locally to make sure JES is using the latest version (of the given tag); defaults to `True` (in practice, App images should never be re-uploaded with the same tag, though)
* `JES_DOCKER_GPU_DRIVER` is the driver for requesting GPU device, e.g. `nvidia`; without this, the App images can not make use of GPU computing resources (but should still be able to run), and if it is set but not available, the Docker containers won't start; defaults to `None`. Multiple drivers can be passed, separated by `;`, one for each Docker host (see above).
* `JES_DOCKER_GPU_COUNT` is the maximum number of individual GPUs allowed for one Job to use in case no explicit GPU Device IDs are specified for a given Worker.
* `JES_DOCKER_RECONNECT_SEC` defines a reconnect-interval (default: 60 seconds); without this, the `aiodocker` client used for awaiting containers may not notice when the connection to a remote Docker host is lost and wait forever.

### Kubernetes
* `JES_KUBERNETES_NAMESPACE` specifies which Kubernetes namespace (if any) the JES should use; if this is not set, it will use Docker instead. Note that a matching Kubernetes Config file has to be installed, as well.
* `JES_KUBERNETES_CONFIG_PATH` Optional path to the Kubernetes Config file. If no value is provided, the configuration at `~/.kube/config` is used. Use this to switch between configurations or load a configuration from a Docker volume.
* `JES_KUBERNETES_GPU_DRIVER` is the driver for requesting GPU device, e.g. `nvidia.com/gpu`; other than the corresponding variable for docker, this does not restrict what the container may use, but which node to run on (one that supports this driver); defaults to `None`. Is the kubernetes-specific equivalent to `JES_DOCKER_GPU_DRIVER` (without the possibility to set drivers for different hosts)
* `JES_KUBERNETES_ALWAYS_PULL` analogous to `JES_DOCKER_ALWAYS_PULL`, for backwards-compatibility instead of renaming existing var
* `JES_KUBERNETES_NODE_LABEL_VALUE` defines the value to the key "empaia" of the node label. This variable can be used to define specific worker nodes which should exclusively be used by the JES. For this reason, these nodes require a [label](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/) (i.e. `kubectl label node <node-name> empaia:<JES_KUBERNETES_NODE_LABEL_VALUE>`) and a [taint](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/) (i.e. `kubectl taint node <node-name> empaia:<JES_KUBERNETES_NODE_LABEL_VALUE>:NoSchedule`). The former adds a node label (i.e. `empaia:<JES_KUBERNETES_NODE_LABEL_VALUE>`) which can be used by the node selector, the latter adds a taint restricting the usage of these nodes. If not set, no node selector and no tolerations are set.

### Authentication
* `JES_IDP_URL` for authorization, URL to use for the identity provider. For Unit Tests, this part is mocked.
* `JES_OPENAPI_TOKEN_URL` (**required**): Full token URL for OpenAPI docs. This frontend URL might differ from `JES_IDP_URL`.
* `JES_CLIENT_ID` (**required**): The Client ID used for authentication at the Marketplace Service; has to match the `JES_AUDIENCE`
* `JES_CLIENT_SECRET` (**required**): The Client Secret used for authentication at the Marketplace Service; has to match the `JES_AUDIENCE`
* `JES_AUDIENCE` (**required**)  for authorization, the audience for this service, e.g. `org.empaia.ci.jes` for CI
* `JES_ENABLE_RECEIVER_AUTH` whether to require authorization for being able to use the service; defaults to `False`
* `JES_REWRITE_URL_IN_WELLKNOWN` when `JES_ENABLE_RECEIVER_AUTH="True`, this overwrites the url received in wellknown used for auth- and token-url, e.g. `https://idp-url-here/auth/realms/EMPAIA`
* `JES_REFRESH_INTERVAL`: Interval in seconds in which a new access token will be fetched from the identification provider service (default: `300`, i.e. 5 minutes)

### Interaction with Marketplace Service and Docker Registries
* `JES_MPS_URL` (**required**): The URL where to find the Marketplace Services, for retrieving the Docker Image name for a given
  EMPAIA App. For Unit Tests, this is mocked.
* `JES_REGISTRY_NAMES`: The Docker Registries where to actually `docker pull` the above Docker Image for the EMPAIA Apps from. This has to exactly match the segment before the first `/` in the Docker image name, e.g. `hub.cc-asp.fraunhofer.de` if the image names have the format `hub.cc-asp.fraunhofer.de/empaia/empaia-app-xyz`. Multiple registries can be separated with `;`. Registry names in this variable are matched with usernames and passwords in the following two variables that are at the same position in the `;`-separated list. If no registry is given, then only images hosted on public registries can be pulled.
* `JES_REGISTRY_LOGINS`: The usernames required for the above Docker Registries for pulling images, separated with `;`. (The n-th username will be used for the n-th Docker registry.)
* `JES_REGISTRY_PWDS`: The passwords required for the above Docker Registries for pulling images, separated with `;`. (The n-th password will be used for the n-th Docker registry.)
* `JES_REGISTRY_SEPARATOR`: Separator to use for the above properties; defaults to `;`, but can be changed to a
  different symbol if that clashes with one of the credentials.
* `JES_IS_COMPUTE_PROVIDER`: Whether the JES is running in a cloud for different WBS (`True`) or on-premise for a single
  WBS (`False`); determines hot to interact with the MPS
* `JES_OWN_ORGANIZATION_ID`: JES's own organization ID, if JES does not act as a compute provider but is running
  on-premise (see `JES_IS_COMPUTE_PROVIDER`); default is None
* `JES_APP_REQUIREMENTS_FILE`: Temporary location of the app requirements JSON file inside the Docker image.

### Logging
* `JES_TOPIC_LABEL`: should be `empaia` (which is also the default) in order for logs to be sent to the logging service; all other values will be ignored
* `JES_LOG_LABEL`: label added to all log messages for differentiation in the logging service; can be chosen freely, but should indicate from which instance of the JES the logs are coming, e.g. `jes-staging`; default is `jes-unknown`


Also, the values of `proxies/default`, if any, in `~/.docker/config.json` will be passed on to the App containers.
In case any of the `JES_DOCKER_EXTRA_ENV_VARS` have the same name as one of the proxy variables, the "extra" value will
be used, which again would be overridden by the environment variables defined by the job, i.e. `EMPAIA_APP_API`,
`EMPAIA_JOB_ID` and `EMPAIA_TOKEN`, should one of those names be used.

For most of those values you can find suitable example values in the `sample.env` file. You will have to add the values
for the MPS and IDP URLs as well as the authentication credentials, though, if you are using the actual services here.
Variables missing in that file may also have a working default value directly defined in the settings module.

### Authentication Credentials for Unit Tests and CI

Credentials are needed in the unit tests for (a) testing how the JES connects to the Marketplace Service to get an
App image, and (b) for testing the authentication of the JES itself.  The credentials needed for this authentication
should _not_ be stored in the Git repository (i.e., anywhere in the code or in the `pyproject.toml`)!

Instead, you can store them in your local `.env` file for running unit tests locally, or in GitLab variables for CI.
The relevant variables are `JES_CLIENT_ID`, `JES_CLIENT_SECRET`, `JES_REGISTRY_LOGINS`, and `JES_REGISTRY_PWDS`,
as well as `WBS_CLIENT_ID` and `WBS_CIENT_SECRET` for testing the authentication of the JES itself. For the Mock MPS and IDP services, all but the Docker credentials are already provided in `sample.env`.


## Container Client Implementations

JES defines a `JobContainerClient` interface that can be implemented for different container backends, such as plain
Docker, Kubernetes, or others. The default is using Docker; Kubernetes is currently in testing.

Following are some notes on the inner working and things to look out to in the different implementations.

### Docker

By default, the JES will run the Jobs using Docker, either using the local Docker instance or a Remote Docker, if the
`JES_DOCKER_HOST` environment variable is set. It uses the `aiodocker` API to communicate with Docker, for pulling
images, starting and monitoring containers (i.e. Jobs), etc.
* Jobs are automatically configures to use the EMPAIA Log format (with `empaia` tag and labels etc.), but a separate
  Promtail instance has to be started on any remote Docker JES is using (see Logging section in this Readme).

#### Multiple Remote Docker Hosts

To provide multiple remote Docker hosts, separate them by `;`, e.g. `JES_DOCKER_HOST=tcp://gpu1:2375;tcp://gpu2:2375;`,
meaning jobs can be run on the servers `gpu1`, `gpu2`, and on the local Docker (the latter coming from the empty string after the trailing `;`). Since each Docker host
may have different GPU capabilities, a matching number of `;`-separated GPU drivers have to be provided via the
`JES_DOCKER_GPU_DRIVER` variable (incl. possibly "no driver" using the empty string).

Which of those Docker hosts is used for a specific Job is determined by the job's execution mode, app requirements, and the Worker that is thus selected. See the section above for how to configure the different Worker queues.

### Kubernetes

If the `JES_KUBERNETES_NAMESPACE` environment variable is set, JES will use Kubernetes instead of Docker. Note that
besides this variable, there also has to be a matching Configuration file installed on the system for connection and
authentication information (see `JES_KUBERNETES_CONFIG_PATH`).
* the `JES_KUBERNETES_GPU_DRIVER` environment variable is used to specify the GPU driver to be used, although that
  may be replaced by a setting in the App's EAD later. Other than Docker, the setting does not restrict what
  the container is allowed to use, but which node to select, so if _all_ nodes support GPU, then the Jobs may even use
  GPU if the variable is not set (and if it is set, any node not providing that driver will not be used).
* For each Job, JES will create a Kubernetes Job (and implicitly a Pod) in the given Kubernetes namespace. When the Jobs
  are stopped, the Kubernetes Pods and Jobs are immediately cleared up (being the only way to stop a Job prematurely).
* Other than using Docker, Logging has to be configured in Kubernetes itself. Instructions for that will follow.


## Setup for Developers

Clone the JES project and submodules with `git clone --recurse-submodules git@gitlab.com:empaia/services/job-execution-service.git`
- a regular `git clone` will not take the provided submodules into account.

Create a file named `.env` in the project directory and paste in the contents of the `Run with docker-compose` chapter 
environment variables. Add your authentication details at the bottom if required.

### Run with Uvicorn

The App is executed with [Uvicorn](https://www.uvicorn.org/) which has to be installed first.

    pip3 install uvicorn

Then run the App with Uvicorn:

    uvicorn job_execution_service.main:app --host 0.0.0.0 --port 8000

Note: When running with Uvicorn, you might have to run `poetry install` once first so that the JES
can "import itself" in order to retrieve its own version number in `__init__.py`.
Also, this does start only the JES, but neither the Data Base nor the Marketplace Service Mock.
In order to have a shorter development cycle, you may want to start the `docker-compose` (see below),
to start the supplementary services, then `docker kill` the JES itself and re-start it with Uvicorn.


### Build with Docker

Build the image with `docker build -t job-execution-service .`.

Do not run the image directly but with `docker-compose` as described further below.

Since the Job Execution Service will execute the EMPAIA Apps as Docker containers, running the JES
itself in Docker can be problematic. If the `DOCKER_HOST` environment variable is used to execute
the Apps on a remote Docker host, this is not a problem. Otherwise, there are different ways to
run the Apps either in the same Docker that executes the JES itself, or in a nested Docker.

See, e.g., here <https://devopscube.com/run-docker-in-docker/> for a discussion and some examples.

#### Mounting `docker.sock`

The simplest way seems to be to mount `docker.sock` as a volume when running the Docker container
or in the `docker-compose.yml`:
```
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
```

This way, the same `docker-compose.yml` can be used whether the Apps are to be executed in the same
Docker, or on a remote host using the `DOCKER_HOST` environment variable.
This is also how it is currently configured in this repository.

If App Service runs on the same host (also via Docker) then the apps, but in a different network, make also use of the
ENV variable `JES_DOCKER_NETWORK`, so apps can connect to the app-service.

#### Using `docker:dind`

If running the App images in the same Docker as the JES itself is not an option, an alternative is to use `docker:dind`
(Docker in Docker) as a separate Service the JES `depends_on`, linking the certificates via a `volume` and
setting the `DOCKER_HOST` and `DOCKER_TLS_CERTDIR` variables accordingly.

```
  dind-service:
    image: docker:dind
    privileged: True
    volumes:
      - dind-certs:/certs
```

However, this method did not entirely work for us, it is more complex and requires a separate configuration for whether
to use the nested Docker, or a remote Docker host. Thus, unless there are specific reasons against it, we recommend using
the first variant.

### Run with docker-compose

Use `docker-compose` to run the Job-Execution-Service with all variables set (including above-mentioned Docker redirect)
and along with a MongoDB and Marketplace Service Mock:

```
docker-compose up --build
```


### Running Unit Tests

The Unit Tests do no longer use mockups but test against a real instance of the Job-Execution-Service.
This means that before running the tests, you have to set up and start a JES instance, either using Docker
compose or just Uvicorn (the latter being a bit faster for local development testing).

```
<copy/create .env file>
python3 -m venv .venv
source .venv/bin/activate
poetry install
uvicorn job_execution_service.main:app

poetry run pytest -vs job_execution_service/test/test_main.py
```

When in doubt, check the `.gitlab-ci.yml` file for reference. If you copy/update your `.env` from the `sample.env`
files, make sure to save your login credentials, which  are not part of the `sample.env`.

Note that while now  all tests use real Docker apps for testing, the tests are still divided into "with auth" and
"without auth", because there are certain features that are easier tested without auth (in particular the same
client-id used for creating and accessing an execution). Thus, make sure to use the correct `.env` parameters when
running those.

### Running Code Checks

The CI will perform code checks and the build will fail if any of those checks do not pass. Please perform the
same checks before pushing your code. Code-checks are only necessary for the actual `job_execution_service`.

```
poetry run black job_execution_service
poetry run isort job_execution_service
poetry run pycodestyle job_execution_service
poetry run pylint job_execution_service
```
