FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.3.0@sha256:dd6c01da098f1cd8863dcaa882fcc9adcca832f9ea1dfa22fb44f952240c09a3 AS builder
COPY . /job-execution-service-mock
WORKDIR /job-execution-service-mock
RUN poetry build && poetry export --without-hashes -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.2.13@sha256:917dd514e96e54b55d729477b440a081afb1892067d6c7f7271188b56c34de04
USER root
COPY --from=builder /job-execution-service-mock/requirements.txt /artifacts
RUN pip install -r /artifacts/requirements.txt
COPY --from=builder /job-execution-service-mock/dist /artifacts
COPY ./app_requirements.json /artifacts/
RUN pip install /artifacts/*.whl
EXPOSE 8000
ENTRYPOINT [ "uvicorn", "job_execution_service.main:app", "--host", "0.0.0.0", "--port", "8000" ]
