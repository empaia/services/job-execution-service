# Changelog

## 0.6.13

* renovate

## 0.6.12

* renovate

## 0.6.11

* renovate

## 0.6.10

* renovate

## 0.6.9

* renovate: isort

## 0.6.8

* extended readme, clarification on docker-registries environment variable

## 0.6.7

* renovate: aiohttp, cryptography, pymongo

## 0.6.6

* renovate: aiohttp, k8s, pydantic

## 0.6.5

* renovate: black

## 0.6.4

* renovate: fastapi, pymongo, uvicorn

## 0.6.3

* renovate: cryptography, black, pytest

## 0.6.2

* renovate: fastapi

## 0.6.1

* renovate: pycodestyle, mps-mock

## 0.6.0

* introduce multiple job queues with dedicated workers for standalone/preprocessing, GPU/CPU, etc.
* allow multiple docker-hosts to be used by different worker

## 0.5.59

* renovate: updated aiohttp, kubernetes, pylint

## 0.5.58

* renovate: updated cryptography, fastapi, kubernetes, pydantic, pylint

## 0.5.57

* renovate: updated black, promtail

## 0.5.56

* edit renovate.json, added 2nd assignee

## 0.5.55

*  Introduce node selector and toleration feature for job scheduling in kubernetes

## 0.5.54

*  fix potential ha proxy earlier timeout (which is an optional deployment for a kubernetes cluster)

## 0.5.53

* upgrade black, grafana and pytest

## 0.5.52

* Remove support for old Solution Store API

## 0.5.51

* upgrade base images

## 0.5.50

* upgrade fastapi, pytest and base images

## 0.5.49

* upgrade fastapi, kubernetes-asyncio, pydantic, pymongo and pytest-env

## 0.5.48

* upgrade fastapi and pydantic


## 0.5.47

* Improve environmental variable parsing

## 0.5.46

* upgrade grafana/promtail
* fix certifi vulnerability

## 0.5.45

* upgrade cryptoprahpy, fastapi, uvicorn nad base image version

## 0.5.44

* upgrade PyJWT, aiohttp, black, cryptography, fastapi, grafana/promtail, pycodestyle, pydantic, pydantic-settings, pylint, pymongo, uvicorn and base image version

## 0.5.43

* upgrade to pydantic version 2.0.0

## 0.5.42

* compare Organization-ID header with value in token, handle error cases

## 0.5.41

* upgrade fastapi after openapi issue

## 0.5.40

* renovate (fastapi, pydantic)

## 0.5.39

* renovate (updated base image version, PyJWT, cryptography, fastapi, grafana/promtail, kubernetes-asyncio, pydantic, pylint, pymongo, pytest, requests, uvicorn)

## 0.5.38

* properly close async Docker client when shutting down JES

## 0.5.37

* renovate (updated base image version, cryptography, fastapi, grafana, pydantic, pylint)

## 0.5.36

* bugfix for 0.5.35

## 0.5.35

* added ENV vars:
  * `JES_DOCKER_GPU_DEVICE_IDS` e.g. "0;1" for 2 GPUs. Availabe gpu devices on the docker host (ignored if GPU driver is not set) (ignores docker_gpu_count). default is None.
  * `JES_DOCKER_GPU_DEVICES_PER_JOB`: use together with [docker_gpu_device_ids]. LEN[docker_gpu_device_ids] * docker_gpu_device_per_job MUST be equal [max_number_of_jobs]. default is None.

## 0.5.34

* renovate (updated base image version, fastapi, uvicorn)

## 0.5.33

* change clean up interval unit from minutes to seconds
* set minimum clean up interval to 60 seconds

## 0.5.32

* fixed bug with sender-auth update-token coroutine never being started

## 0.5.31

* updated receiver auth submodule to fix uncaught decode error

## 0.5.30

* added number of scheduled and running executions to status

## 0.5.29

* renovate (updated base image version, PyJWT, cryptography, isort, pycodestyle, pydantic, pylint, pymongo, pytest, requests)

## 0.5.28

* updated auth submodules

## 0.5.27

* renovate (updated base image version)

## 0.5.26

* introduce new execution db data volume for data persistence

## 0.5.25

* renovate (updated base image version, fastapi, aiohttp)

## 0.5.24

* renovate (updated base image version, fastapi, grafana/promtail)

## 0.5.23

* renovate (updated base image version, black)

## 0.5.22

* create new worker whose only task is to clean up app container/pods
* create new clean_up method for container client
* create clean_up methods for docker and k8s container clients

## 0.5.21

* renovate (updated base image version)

## 0.5.20

* Improve ReadMe structure related to logging and promtail
* Introduce daemonset (+ secrets, configmap, serviceaccount, clusterrole and clusterrolebinding) template for kubernetes

## 0.5.19

* make k8s image pull policy adaptive (either "always" or "ifnotpresent")
  
## 0.5.18

* fix too long pod labels; caused by long (>63) image name

## 0.5.17

* add pod labels (JES K8 support) which will be used by promtail daemonset
* avoid deleting jobs/pods which fail or succeed for now; will be re-established differently in a future version

## 0.5.16

* renovate (updated base image, promtail version)
  

## 0.5.15

* fix delete-pod method to handle case of terminated pod was already deleted when deleting the job
  
## 0.5.14

* introduce k8 specific gpu driver env-variable
  
## 0.5.13

* considerably extended jes-test-app

## 0.5.12

* renovate (updated base image, cryptography, fastapi, grafana/promtail version)

## 0.5.11

* renovate (updated base image)

## 0.5.10

* renovate (updated base image)

## 0.5.9

* renovate (updated base image, fastapi versions)

## 0.5.8

* renovate (updated base image, fastapi, uvicorn versions)
* adapted to new MPS Mock no longer supporting v0 routes, using v1 in unit tests now

## 0.5.7

* allow empty docker gpu driver variable

## 0.5.6

* allow for multiple Remote Docker Clients to be defined using semicolon-separated values

## 0.5.5

* renovate (updated base image, uvicorn, pytest versions)

## 0.5.4

* explicitly creating instances of clients instead of cached get-functions
* simplified interface of container clients (fewer functions) -> some code duplication but better flexibility

## 0.5.3

* renovate (updated base image version)

## 0.5.2

* renovate (updated base image version)

## 0.5.1

* renovate (updated base image version, aiohttp, k8s)

## 0.5.0

* implemented Container Client for Kubernetes

## 0.4.25

* renovate (updated base image version, fastapi, cryptography)

## 0.4.24

* renovate (updated base image version)

## 0.4.23

* renovate

## 0.4.22

* renovate

## 0.4.21

* renovate

## 0.4.20

* renovate

## 0.4.19

* renovate

## 0.4.18

* changed user-id header to organization-id
* added mps organization header for MPS v1

## 0.4.17

* renovate

## 0.4.16

* added environment variable / Setting for GPU count

## 0.4.15

* convert Docker proxy settings from camelCase to both snake_case and UPPER_CASE before passing to container
* added `JES_DOCKER_EXTRA_ENV_VARS` environment variable for passing additional env vars to app containers

## 0.4.14

* added error log

## 0.4.13

* added docker extra hosts setting

## 0.4.12

* renovate
* updated MPS mock env variables and paths

## 0.4.11

* updated dependencies major version and adapted DB code

## 0.4.10

* specify number of GPUs to use (default worked before, but does not on new server)

## 0.4.9

* updated mps mock to v1 routes

## 0.4.8

* updated dependencies

## 0.4.7

* updated ci

## 0.4.6

* use Marketplace Service (MPS) Mock instead of old VG Solution Store Service
* renamed some environment variables accordingly

## 0.4.5

* allow to pass credentials for multiple Docker registries
* fixed bug with checking if image already exists

## 0.4.4

* (unintended version bump)

## 0.4.3

* get proxy settings from docker-config and add them to container env

## 0.4.2

* fixed bytes vs string error in case no Docker registry is specified

## 0.4.1

* use Docker Auth only if pulling the image from that same repository, not public ones

## 0.4.0

* separate attributes for exit-code and message
* updated execution states (merged COMPLETED and FAILED to TERMINATED, added STOPPED)

## 0.3.26

* fix for 0.3.25

## 0.3.25

* updated receiver-auth
* added setting to rewrite URL in wellknown

## 0.3.24

* big refactoring of code structure
* towards implementation of async methods

## 0.3.23

* update empaia-receiver-auth
* added new env var JES_OPENAPI_TOKEN_URL

## 0.3.22

* described Logging related Env vars in readme and addressed Logging with Apps on remote Docker

## 0.3.21

* improved logging

## 0.3.20

* configurable token URL path

## 0.3.19

* added configuration for Docker to format logs and with that enable the tagging pipeline with Loki

## 0.3.18

* going open-source on Gitlab.com, added license, updated submodules, etc.

## 0.3.17

* changed all Unit Tests to test against a real JES instance instead of Mockups

## 0.3.16

* added route `/v1/executions/{job_id}/stop` to stop scheduled or running jobs

## 0.3.15

* added flag to enable globally Docker GPU support; for now, this means each Job will be executed
  with GPU support; later this can be differentiated with GPU requirements found in the EAD

## 0.3.14

* more differentiated error status codes on POST and GET
  * 412 if mismatched user token, 404 if app/job not found, 409 if job already exists
 
## 0.3.13

* fix faulty and no longer needed treatment for image names starting with "https://"

## 0.3.12

* use Job ID for container name to allow for `docker logs <job_id>`

## 0.3.11

* fixed exception handling bug with using new sender-auth, updated test app image name

## 0.3.10

* added user-id header and add it to execution object and check again when accessing the status
  to make sure only job creator can access execution status

## 0.3.9

* updated Dockerfile to build and install from Wheel to get proper JES Version number

## 0.3.8

* using dynamic wait-for-status loops in Unit Tests to prevent occasional failed tests, 
  adapted wait-times where this is not possible
* added flag whether to always re-pull docker images (default true; false for unit tests)

## 0.3.7

* Always pull app images when executing (eases testing procedure)

## 0.3.6

* using `empaia-sender-auth` module for lazy authentication

## 0.3.5

* renamed env variables `JES_SSS_CLIENT_ID` and `JES_SSS_CLIENT_SECRET` to just `JES_CLIENT_ID` and `JES_CLIENT_SECRET`
* show `ErrorCode` in `error_message` if Docker container does not provide an `Error` string in status

## 0.3.4

* renamed `ExecutionRequest` to `PostExecution` and extend `ExecutionStatus` to `Execution(PostExecution)`,
  i.e., including all the request attributes
* return full `Execution` as result to POST, and return full `Execution` except Access Token (blanked out) on GET
* since only attributes are added, old client code may still work
* added new `alive` route like most other EMPAIA services have, too

## 0.3.3

* added alternative "Event-Based" Runner in addition to the current Looping Runner
* separated (Docker-)Client from Runner code so both can be varied and combined independently of each other
* adapted code in other places, unit tests and readme accordingly

## 0.3.2

* Renamed `JES_USE_AUTHENTICAITON` to:
  * `JES_ENABLE_RECEIVER_AUTH` whether to require authorization for being able to use the service; defaults to `False`
* Added env var:
  * `JES_ENABLE_SENDER_AUTH` whether to fetch a token from `JES_IDP_URL` before sending any outgoing request; defaults to `False`

## 0.3.1

* changed routes to plural, i.e. `/v1/execution/*` to `/v1/executions/*`

## 0.3.0

* added `/v1` prefix to routes
* changed routes to `POST /v1/execution` and `GET /v1/execution/{job-id}`
* changed `JobStatus` to `ExecutionStatus`

## 0.2.8

* updated ID of JES Test App
* added unit test to test connectivity to SSS independently of any specific App

## 0.2.7

* update Auth to new credentials, get credentials from local .env

## 0.2.6

* improved API Documentation

## 0.2.5.

* added env var to specify docker network

## 0.2.4.

* added parameter for database name

## 0.2.3

* get Docker images from Solution Store Service

## 0.2.2

* implemented authentication

## 0.2.1

* implemented Job queue

## 0.2.0

* adapted to new job-service

## 0.1.0

* very first version, duplicating some functionality of job-service
