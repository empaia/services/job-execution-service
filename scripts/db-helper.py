"""
This is _not_ part of JES. Just a simple tool for inspecting and cleaning the DB,
e.g. after excessive unit testing or for checking DB functionality, migrations, etc.
"""

import json
import uuid

import pymongo

db_url = "mongodb://localhost:27017/"
db_name = "job-execution-service-test-db"
client = pymongo.MongoClient(db_url, uuidRepresentation="standard")
exec_db = client[db_name]["executions"]
cont_db = client[db_name]["job-container"]


def show(item):
    item["_id"] = str(item["_id"])
    return json.dumps(item, indent=2)


print("Super simple JES DB helper script")
print("Commands: list, job ID, cont ID, delete ID, purge, exit")
print(f"Using {db_name} @ {db_url}")

while True:
    if not (args := input(">>> ").split()):
        print("type 'exit' to exit")
        continue
    try:
        cmd, id = args[0], args[1] if len(args) > 1 else None
        if cmd == "list":
            items = [show(x) if "full" in args[1:] else str(x["_id"]) for x in exec_db.find({})]
            print(*items, sep="\n")
            print(len(items))
        elif cmd == "job" and id:
            print(show(exec_db.find_one({"_id": uuid.UUID(id)})))
        elif cmd == "cont" and id:
            print(show(cont_db.find_one({"_id": uuid.UUID(id)})))
        elif cmd == "delete" and id:
            exec_db.delete_one({"_id": uuid.UUID(id)})
            cont_db.delete_one({"_id": uuid.UUID(id)})
        elif cmd == "purge" and input("Purge DB? (y/N) ") == "y":
            exec_db.drop()
            cont_db.drop()
        elif cmd == "exit":
            break
        else:
            print("unknown command:", cmd)
    except Exception as e:
        print("ERROR", e)
