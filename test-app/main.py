import os
import time

import requests


def main():
    print("Running JES Test App. Pass 'help' as EMPAIA_TOKEN env var for help.")
    access_token = os.getenv("EMPAIA_TOKEN", "")

    print(f"EMPAIA_TOKEN: '{access_token}'")
    parameters = iter(access_token.split())
    for cmd in map(str.lower, parameters):
        if cmd == "env":
            print_env()
        elif cmd == "fail":
            fail(int(next(parameters, 1)))
        elif cmd == "get":
            get_url(next(parameters))
        elif cmd == "gpu":
            gpu_test_tensorflow(int(next(parameters)))
        elif cmd == "help":
            help()
        elif cmd == "memory":
            memory_test(int(next(parameters)))
        elif cmd == "wait":
            wait(int(next(parameters)))
    print("Done")


def help():
    with open("README.md") as f:
        print(f.read())


def print_env():
    print("Environment Variables:")
    for key, val in sorted(os.environ.items()):
        print(f"- {key}={val}")


def wait(seconds: int):
    print(f"Waiting {seconds} seconds...")
    for i in range(seconds):
        time.sleep(1)
        print(f"- {i + 1}/{seconds}", flush=True)


def fail(code: int):
    print(f"Failing with exit code {code}...")
    exit(code)


def gpu_test_cupy(size: int, iters=100):
    print("Testing GPU with CuPy")
    try:
        import cupy as cp

        s = cp.cuda.Stream()
        s.use()  # any subsequent operations are done on steam s
    except Exception as e:
        print("- Could not import cupy, using numpy instead:", e)
        import numpy as cp

    print(f"- Creating matrices of size {size}x{size}")
    A, B = (cp.random.random((size, size)) for _ in range(2))
    print(f"- Doing {iters} matrix multiplications...")
    start = time.time()
    for _ in range(iters):
        B = A * B
    print(f"- Time: {time.time() - start}")

    try:
        cp.cuda.Stream.null.use()  # fall back to the default (null) stream
    except Exception as e:
        pass


def gpu_test_torch(size: int, iters=100):
    print("Testing GPU with PyTorch")
    import torch

    print(f"- Creating matrices of size {size}x{size}")
    A, B = (torch.rand(size, size) for _ in range(2))
    try:
        A, B = A.cuda(), B.cuda()
    except RuntimeError as e:
        print("- Failed to use CUDA:", e)

    print(f"- Doing {iters} matrix multiplications...")
    start = time.time()
    for _ in range(iters):
        B = A * B
    print(f"- Time: {time.time() - start}")


def gpu_test_tensorflow(size: int, iters=100):
    print("Testing GPU with TensorFlow")
    import tensorflow as tf

    physical_devices = tf.config.list_physical_devices("GPU")
    gpu_available = len(physical_devices) > 0
    if not gpu_available:
        print("- No GPUs available, using CPU instead")

    print(f"- Creating matrices of size {size}x{size}")
    A, B = (tf.random.uniform((size, size)) for _ in range(2))

    print(f"- Doing {iters} matrix multiplications...")
    start = time.time()
    for _ in range(iters):
        B = A * B
    print(f"- Time: {time.time() - start}")


def memory_test(size: int):
    print(f"Testing Memory allocation for {size} MB")
    # TODO try to allocate specific amount of memory instead
    print("- not yet implemented")


def get_url(url: str):
    print(f"Trying to connect to external URL {url}")
    res = requests.get(url)
    print("- Status code:", res.status_code)


if __name__ == "__main__":
    main()
