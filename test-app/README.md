# JES Test App

Super-simple App for testing the Job Execution-Service. The app does nothing and
does not interact with the App Service. It can thus be started just from the JES,
without requiring an App Service or Medical Data Service to run.

It is intended to test the JES by simulating different app behaviours, e.g. running
for some time, using GPU, crashing, etc. Its behavior is controlled via commands 
given in the `EMPAIA_TOKEN` Environment Variable passed to the app. The other
variables, `EMPAIA_JOB_ID` and `EMPAIA_APP_API`, are not used and can have any value.

## Commands / Test Modes

Different modes are activated using commands that can be chained together:

- `help`: Print this `README.md` file
- `env`: Print all Environment Variables
- `wait [s: int]`: Wait for `s` seconds
- `gpu [n: int]`: Try to use GPU to do 100 multiplications of two `n`x`n` matrices
- `memory [n: int]`: Try to allocate `n` MB of memory (not yet implemented)
- `get [url: str]`: Try to connect to some (external) URL
- `fail [c: int?]`: Exit with status code `c` (default: 1)

For example, passing the following value as `EMPAIA_TOKEN` will make the app `wait`
for 3 seconds, then print its `env`, then try to use GPU to multiply two 1000x1000
matrices, then fail with exit code 1:

```
"wait 3 env gpu 1000 fail"
```

## Building with Docker 

Build with Docker:

```
docker build -t jes-test-app .
```

(Unfortunately, the CUDA support seems to make the image _very_ large; another
base image might be smaller; if you do not want to test GPU, you might also just
use `python:slim` as base image without installing any other packages))

## Running the App

Run from command line, e.g. to test the `jes-test-app` itself, or for testing
GPU support of the Docker host (add `--gpus all` for enabling GPU support and use
`DOCKER_HOST` or `docker context` to connect to remote docker if needed):

```
docker run -e EMPAIA_TOKEN="wait 3 env gpu 1000 fail" jes-test-app
```

Run with JES to test the Job-Execution-Service and its configuration, e.g. for GPU
support. The `EMPAIA_TOKEN` is passed in the `access_token` parameter. This requires
the `jes-test-app` to be known to the JES, by either:

- properly deploying the `jes-test-app` to the EMPAIA app image registry
  and adding an appropriate EAD in the marketplace service
- building the `jes-test-app` locally and adding an entry in `mps-mock-data/apps.json`
- patching the `get_app_image_name` in JES's `mps_client.py` to just return the
  `app_id` itself as the app's `image_name`

```
{
  "app_id": "app-id-point-the-jes-to-the-jes-test-app",
  "job_id": "cf0c35e9-7bee-4fc9-a5f2-9ac8e7fbbe30",
  "access_token": "wait 3 env gpu 1000 fail",
  "app_service_url": "https://does-not-matter.com",
  "timeout": 5
}
```
