"""
Module for executing jobs on Docker (locally or remotely).
"""

import asyncio
import asyncio.exceptions
from typing import Optional

import aiodocker
from aiodocker.containers import DockerContainer

from ..database import NewStatus
from ..model import ExecStatus, Execution, JobContainer
from ..singletons import logger, settings
from . import JobContainerClient, create_env, get_additional_env, get_docker_auth, get_proxy_settings

SEP = ";"


class DockerClient(JobContainerClient):
    """Docker client using docker and aiodocker APIs to connect to local or remote Docker host."""

    def __init__(self):
        # Set DOCKER_HOST from settings. Setting DOCKER_HOST directly seems to be problematic with docker-compose,
        # as it seems to end up as an empty string instead of None, which is a problem for aiodocker.
        docker_hosts = settings.docker_host.split(SEP)
        logger.info("Using Docker on %s", docker_hosts or "this machine")
        assert len(docker_hosts) == len(settings.docker_gpu_driver.split(SEP))

        self.aioclients = {host: aiodocker.Docker(url=host or None) for host in docker_hosts}
        self.auth = get_docker_auth()
        self.proxies = get_proxy_settings()
        self.extra_env = get_additional_env()
        self.running = True

    async def teardown(self):
        self.running = False
        for host, client in self.aioclients.items():
            logger.info("Disconnecting from Docker Host %s", host)
            await client.close()

    async def execute_job(self, job_ex: Execution, image_name: str, host: str, use_gpu: bool, devices: str) -> str:
        if host not in self.aioclients:
            raise RuntimeError(f"Unknown Docker Host: {host}")
        client = self.aioclients[host]
        logger.info("Running Job on %s", client.docker_host)

        if settings.docker_always_pull or not await self._has_image(client, image_name):
            logger.info("Pulling %s...", image_name)
            # only use auth if auth set and image is from that repository, otherwise assume no auth needed
            reg, *_ = image_name.split("/")
            await client.images.pull(image_name, auth=self.auth.get(reg))

        # assemble container configuration
        host_config = {}
        env = {**self.proxies, **self.extra_env, **create_env(job_ex)}
        config = {
            "Image": image_name,
            "Env": [f"{k}={v}" for k, v in env.items()],
            "HostConfig": host_config,
        }
        if settings.log_label:
            host_config["LogConfig"] = {"Type": "json-file", "Config": get_log_config(job_ex)}
        if use_gpu and (gpu_driver := await self._gpu_driver_for_client(client)):
            request = {
                "Driver": gpu_driver,
                "Capabilities": [["gpu"]],
            }
            if devices:
                request["DeviceIDs"] = devices.split(",")
            else:
                request["Count"] = settings.docker_gpu_count
            host_config["DeviceRequests"] = [request]
        if settings.docker_network:
            host_config["NetworkMode"] = settings.docker_network
            config["NetworkingConfig"] = {settings.docker_network: None}
        if settings.docker_extra_hosts:
            extra_hosts = settings.docker_extra_hosts.split(SEP)
            host_config["ExtraHosts"] = extra_hosts

        # start the container
        container = await client.containers.run(name=job_ex.job_id, config=config)
        return container.id

    async def kill_job(self, job_cont: JobContainer) -> bool:
        client = await self._with_container(job_cont.container_id)
        try:
            container = await client.containers.get(job_cont.container_id)
            await container.kill()  # may fail if Container finished just now
            return True
        except aiodocker.DockerError as e:
            logger.warning("Could not kill job: %s", e)
            return False

    async def clean_up(self, job_cont: JobContainer) -> bool:
        try:
            client = await self._with_container(job_cont.container_id)
            container = await client.containers.get(job_cont.container_id)
            await container.delete()
            return True
        except Exception as e:
            logger.warning("Could not clean up job: %s", e)
            return False

    async def await_job(self, job_cont: JobContainer, timeout: Optional[int]) -> Optional[NewStatus]:
        # check if container still exists, e.g. on JES restart with new Docker Host
        if (await self._get_container(job_cont)) is None:
            return NewStatus(ExecStatus.ERROR, None, "Container could not be found")

        client = await self._with_container(job_cont.container_id)
        # wait for the timeout or as long as the container needs to finish
        if (result := await self._await_job_loop(client, job_cont.container_id, timeout)) is not None:
            if result:
                # job finished normally or was stopped by user: update container and return status
                return await self._get_result(client, job_cont.container_id)
            else:
                # timeout reached: kill job, then return status with modified message indicating timeout
                logger.info("Killing Job %s: Timeout", job_cont.job_id)
                await self.kill_job(job_cont)
                result = await self._get_result(client, job_cont.container_id)
                return NewStatus(ExecStatus.TIMEOUT, result.exit_code, "Job timeout reached.")

    async def _get_container(self, job_cont: JobContainer) -> Optional[DockerContainer]:
        try:
            client = await self._with_container(job_cont.container_id)
            return await client.containers.get(job_cont.container_id)
        except aiodocker.DockerError as e:
            logger.warning("Error getting container %s: %s", job_cont, e)
            return None

    async def _get_result(self, client: aiodocker.Docker, container_id: str) -> NewStatus:
        container = await client.containers.get(container_id)
        container_state = (await container.show())["State"]
        status = ExecStatus.TERMINATED
        exit_code = container_state["ExitCode"]
        message = container_state["Error"] or None  # when exactly, if at all, is this not empty?
        return NewStatus(status, exit_code, message)

    async def _await_job_loop(self, client: aiodocker.Docker, container_id: str, timeout: Optional[int]) -> bool:
        if timeout is not None and timeout <= 0:
            return False

        container = await client.containers.get(container_id)
        # just calling container.wait will wait indefinitely if no timeout is set and connection is lost
        # this way, container.wait will keep running if the connection is lost, but then fail immediately
        # when trying to reconnect in the next iteration of the loop
        reconnect = settings.docker_reconnect_sec
        while self.running:
            wait_time = min(timeout, reconnect) if timeout is not None else reconnect
            try:
                await container.wait(timeout=wait_time)
            except asyncio.exceptions.TimeoutError:
                # check if actual timeout (if any) has passed
                # any other exceptions are raised to Worker Task, resulting in Job Error
                if timeout is not None:
                    timeout -= wait_time
                    if timeout <= 0:
                        return False
            else:
                # container.wait finished without timeout
                return True

    async def _with_container(self, container_id: str) -> aiodocker.Docker:
        """Get next client that has the given docker image, if any, or raise exception."""
        # Alternatively, could also store docker-host in Container DB...
        for client in self.aioclients.values():
            try:
                await client.containers.get(container_id)
                return client
            except aiodocker.DockerError:
                pass
        raise RuntimeError(f"Docker Client with container {container_id} not available or unknown container id.")

    async def _gpu_driver_for_client(self, client: aiodocker.Docker) -> str:
        """Get matching GPU driver for the given Docker client"""
        for host, driver in zip(self.aioclients, settings.docker_gpu_driver.split(SEP)):
            if self.aioclients[host] is client:
                return driver

    async def _has_image(self, client: aiodocker.Docker, image_name: str) -> bool:
        """Check if Image exists on Docker Host; images.list does not work as before."""
        try:
            await client.images.inspect(name=image_name)
            return True
        except aiodocker.exceptions.DockerError:
            return False


def get_log_config(job_ex: Execution) -> dict:
    """Get tags for logging with Promtail/Loki/Grafana"""
    tags = [settings.topic_label, settings.log_label, "{{.ImageName}}", job_ex.job_id, job_ex.app_service_url]
    return {"tag": "|".join(tags)}
