"""
Module for executing jobs on a Kubernetes cluster.
"""

import asyncio
import asyncio.exceptions
import base64
import re
import time
from typing import Optional, Tuple

import kubernetes_asyncio
from kubernetes_asyncio import client as k8s_client

from ..database import NewStatus
from ..model import ExecStatus, Execution, JobContainer
from ..singletons import logger, settings
from . import JobContainerClient, create_env, get_additional_env, get_docker_auth, get_proxy_settings


class KubernetesClient(JobContainerClient):
    """Kubernetes client using kubernetes_asyncio to connect to (remote) Kubernetes host."""

    def __init__(self):
        self.namespace = settings.kubernetes_namespace
        logger.info("Using Kubernetes with namespace %r", self.namespace)

        # initialized later, requiring async/await
        self.api: Optional[k8s_client.api_client.ApiClient] = None
        self.job_api: Optional[k8s_client.BatchV1Api] = None
        self.core_api: Optional[k8s_client.CoreV1Api] = None

        # auth and proxy settings (same as for Docker)
        self.auth = get_docker_auth()
        self.proxies = get_proxy_settings()
        self.extra_env = get_additional_env()

    async def initialize(self):
        logger.info("Connecting to Kubernetes...")
        await kubernetes_asyncio.config.load_kube_config(config_file=settings.kubernetes_config_path or None)
        self.api = k8s_client.api_client.ApiClient()
        self.job_api = k8s_client.BatchV1Api(self.api)
        self.core_api = k8s_client.CoreV1Api(self.api)

        known_secrets = {x.metadata.name for x in (await self.core_api.list_namespaced_secret(self.namespace)).items}
        for reg, values in self.auth.items():
            if reg not in known_secrets:
                lgn_pwd = b64_enc("%s:%s" % (values["username"], values["password"]))
                data = b64_enc('{"auths": {"%s": { "auth": "%s" }}}' % (values["serveraddress"], lgn_pwd))
                secret = k8s_client.V1Secret(
                    metadata=k8s_client.V1ObjectMeta(name=reg),
                    type="kubernetes.io/dockerconfigjson",
                    data={".dockerconfigjson": data},
                )
                await self.core_api.create_namespaced_secret(self.namespace, body=secret)
                logger.info("Created Kubernetes Secret for %s", reg)
            else:
                logger.info("Kubernetes Secret already exists for %s", reg)

    async def teardown(self):
        logger.info("Disconnecting from Kubernetes...")
        await self.api.close()

    async def execute_job(self, job_ex: Execution, image_name: str, host: str, use_gpu: bool, devices: str) -> str:
        # get secret needed to log into Docker registry, if any
        reg, *_ = image_name.split("/")
        secrets = [k8s_client.V1ObjectReference(name=reg)] if reg in self.auth else None

        # GPU resources: other than using Docker, those don't say what the Pod is allowed to use, but where it can run
        # TODO use host and device-ID to select node and GPU devices
        if use_gpu and settings.kubernetes_gpu_driver:
            resources = k8s_client.V1ResourceRequirements(limits={settings.kubernetes_gpu_driver: 1})
        else:
            resources = None

        # assemble container configuration
        env = {**self.proxies, **self.extra_env, **create_env(job_ex)}
        name = f"jes-job-{job_ex.job_id}"
        job_body = k8s_client.V1Job(
            api_version="batch/v1",
            kind="Job",
            metadata=k8s_client.V1ObjectMeta(namespace=self.namespace, name=name),
            spec=k8s_client.V1JobSpec(
                template=k8s_client.V1PodTemplateSpec(
                    metadata=k8s_client.V1ObjectMeta(
                        labels={
                            "topic": settings.topic_label,
                            "log_label": settings.log_label,
                            "image_name": convert_image_name_to_labels(image_name)[0],
                            "image_tag": convert_image_name_to_labels(image_name)[1],
                            "job_id": job_ex.job_id,
                            "service_url": re.sub(r"[^._\w-]", "_", job_ex.app_service_url),
                        },
                    ),
                    spec=k8s_client.V1PodSpec(
                        containers=[
                            k8s_client.V1Container(
                                name=job_ex.job_id,
                                image=image_name,
                                env=[k8s_client.V1EnvVar(name=k, value=v) for k, v in env.items()],
                                image_pull_policy="Always" if settings.kubernetes_always_pull else "IfNotPresent",
                                resources=resources,
                            ),
                        ],
                        node_selector=(
                            {"empaia": settings.kubernetes_node_label_value}
                            if settings.kubernetes_node_label_value is not None
                            else None
                        ),
                        tolerations=(
                            [
                                k8s_client.V1Toleration(
                                    key="empaia",
                                    operator="Equal",
                                    value=settings.kubernetes_node_label_value,
                                    effect="NoSchedule",
                                )
                            ]
                            if settings.kubernetes_node_label_value is not None
                            else None
                        ),
                        image_pull_secrets=secrets,
                        restart_policy="Never",  # no restarts on failed jobs
                    ),
                ),
                backoff_limit=0,  # no, really no restarts!
            ),
        )

        # start the Job
        await self.job_api.create_namespaced_job(self.namespace, job_body)
        return name

    async def kill_job(self, job_cont: JobContainer) -> bool:
        try:
            await self._delete_job(job_cont.container_id, True)
            return True
        except k8s_client.exceptions.ApiException as e:
            logger.warning("Could not kill job: %s", e)
            return False

    async def clean_up(self, job_cont: JobContainer) -> bool:
        try:
            await self._delete_job(job_cont.container_id, kill_pod=False)
            return True
        except k8s_client.exceptions.ApiException:
            logger.warning("Could not clean up job: Container already removed or not found")
            return False

    async def await_job(self, job_cont: JobContainer, timeout: Optional[int]) -> Optional[NewStatus]:
        # allow for some time for container being created if called directly after start
        await asyncio.sleep(1.0)

        # check if container still exists, e.g. on JES restart with new Docker Host
        if (await self._pod_for_job(job_cont.container_id)) is None:
            return NewStatus(ExecStatus.ERROR, None, "Container could not be found")

        # wait for the timeout or as long as the container needs to finish
        if timeout is None or timeout > 0:
            run_until = int(time.time()) + timeout if timeout is not None else -1
            while timeout is None or int(time.time()) < run_until:
                pod_name = await self._pod_for_job(job_cont.container_id)
                # TODO: is this logically correct? just because we do not find the pod name
                # the job must have been cancelled by the user?
                if pod_name is None:
                    return NewStatus(ExecStatus.STOPPED, 137, "Job stopped by user.")
                pod = await self.core_api.read_namespaced_pod(pod_name, self.namespace)
                if pod.status.phase in ("Succeeded", "Failed", "Unknown"):
                    pod_status = pod.status.container_statuses[0].state.terminated
                    return NewStatus(ExecStatus.TERMINATED, pod_status.exit_code, pod_status.message)
                await asyncio.sleep(3.0)
        # timeout reached, either before or while waiting
        logger.info("Killing Job %s: Timeout", job_cont.job_id)
        await self._delete_job(job_cont.container_id, True)
        return NewStatus(ExecStatus.TIMEOUT, None, "Job timeout reached.")

    async def _pod_for_job(self, job_id: str) -> str:
        """get name of pod for given job"""
        pods = await self.core_api.list_namespaced_pod(self.namespace, label_selector=f"job-name={job_id}")
        return pods.items[0].metadata.name if pods.items else None

    async def _delete_job(self, job_id: str, kill_pod=False):
        pod_name = await self._pod_for_job(job_id)
        del_opt = k8s_client.V1DeleteOptions(grace_period_seconds=0, propagation_policy="Foreground")
        # delete job first to prevent restart of pod (if so configured)
        await self.job_api.delete_namespaced_job(job_id, self.namespace, body=del_opt)
        if kill_pod:
            # delete pod (auto-deleted with job if no longer running, thus optional)
            await self.core_api.delete_namespaced_pod(pod_name, self.namespace, body=del_opt)


def b64_enc(s):
    """shorthand to encode to base64 for Kubernetes Secrets"""
    return base64.encodebytes(bytes(s, "utf8")).decode("utf8").replace("\n", "")


def convert_image_name_to_labels(image_name: str) -> Tuple[str, str]:
    """Converts a docker image name to name and tag label

    :param image_name:  the docker registry name
    :returns: tuple[str, str]: name and tag label
    """
    name_and_tag = image_name.split("/")[-1].split("@")[0]
    name = name_and_tag.split(":")[0]
    name = re.sub(r"[^._\w-]", "_", name)
    tag = name_and_tag.split(":")[-1]
    tag = re.sub(r"[^._\w-]", "_", tag)
    tag = tag if tag != name else ""
    return name[:63], tag[:63]
