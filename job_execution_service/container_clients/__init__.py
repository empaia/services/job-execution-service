"""
Module for actually executing the jobs in different ways.

The execution of the jobs is split into several parts:
- The JobRunner determines how Jobs are scheduled, executed, and when and how they are checked for completion
- The JobContainerClient determines how to interact with the actual containers, e.g. using Docker or Kubernetes

This module contains the ContainerClient interface for "low-level" execution of the container using different backends,
like pure Docker, Kubernetes, and others. The (rather simple) scheduling part is in jobexecutor.py.
"""

import json
import os
import re
from typing import Optional

from ..database import NewStatus
from ..model import Execution, JobContainer
from ..singletons import logger, settings


class JobContainerClient:
    """Abstract interface for the JES container management. Depending on the technology it could be implemented using
    asynchronous Python libraries for
      * Docker: https://github.com/aio-libs/aiodocker
      * Nomad:  https://github.com/aio-libs/aiohttp
      * K8s:    https://github.com/tomplus/kubernetes_asyncio
    """

    async def initialize(self):
        """Do async initialization tasks that can not be done in __init__ (optional)."""

    async def teardown(self):
        """Do async teardown tasks, e.g. for disconnecting from server (optional)"""

    async def execute_job(self, job_ex: Execution, image_name: str, host: str, use_gpu: bool, devices: str) -> str:
        """Execute the job as a Docker container, passing job ID, token and endpoint as environment variable.
        The default implementation will delegate the Job to a Docker environment and start the container, but
        the class could also be subclasses and the method overwritten to use e.g. Kubernetes or similar.
        """

    async def kill_job(self, job_cont: JobContainer) -> bool:
        """Kills the job container given its id. Everything else, e.g. finishing the job, updating the database,
        is handled by JobRunner._await_execution."""

    async def clean_up(self, job_cont: JobContainer) -> bool:
        """Cleans up containers for already completed jobs (successful/time-out/killed/failed) from disk."""

    async def await_job(self, job_cont: JobContainer, timeout: Optional[int]) -> Optional[NewStatus]:
        """Wait at most `timeout` seconds for container's completion, then get and return the status from the
        completed container. Everything else, e.g. updating the database, is handled in check_jobs so this part
        can easily be overwritten for a different backend."""


def create_env(job_ex: Execution) -> dict:
    """Create Dictionary holding the different EMPAIA Environment Variables."""
    return {
        "EMPAIA_APP_API": job_ex.app_service_url,
        "EMPAIA_JOB_ID": job_ex.job_id,
        "EMPAIA_TOKEN": job_ex.access_token,
    }


def get_additional_env() -> dict:
    """Get additional environment variables to be passed to App containers"""
    res = {}
    for var in settings.docker_extra_env_vars:
        try:
            key, value = var.split("=", 1)
            res[key] = value
        except ValueError:
            logger.warning("Badly formed extra env var: %s", var)
    return res


def get_docker_auth() -> dict:
    """Get Dict mapping Docker registries to auth credentials from settings"""
    sep = settings.registry_separator
    registries = [re.sub(r"^\w+://", "", reg) for reg in settings.registry_names.split(sep)]
    usernames = settings.registry_logins.split(sep)
    passwords = settings.registry_pwds.split(sep)
    if not (len(registries) == len(usernames) == len(passwords)):
        logger.warning("Mismatched number of Docker Registry names and corresponding logins and passwords!")
    return {
        reg: {
            "username": lgn,
            "password": pwd,
            "serveraddress": reg,
        }
        for (reg, lgn, pwd) in zip(registries, usernames, passwords)
    }


def get_proxy_settings() -> dict:
    """Read and return proxies/default section from .docker/config.json, if it exists."""
    conf_path = f"{os.getenv('HOME')}/.docker/config.json"
    try:
        with open(conf_path, encoding="utf8") as f:
            configured_proxies = json.load(f).get("proxies", {}).get("default", {})
            # convert keys from camelCase to both snake_case and UPPER_CASE, as docker does
            pattern = re.compile(r"(?<=[a-z])(?=[A-Z])")  # zero-width match between lower-case and upper-case
            return {
                new_key: value
                for key, value in configured_proxies.items()
                if key in ("httpProxy", "httpsProxy", "ftpProxy", "noProxy")
                for new_key in [pattern.sub("_", key).lower(), pattern.sub("_", key).upper()]
            }
    except FileNotFoundError:
        logger.warning("%s not found", conf_path)
        return {}
