"""
In the  current design, the Job Execution Service does not connect back to the Job Service in the
Medical Data Service to update the status of the jobs when they are scheduled, running, completed
or failed. Instead, the Job Execution Service.

The Workbench Service will periodically poll the status of the job and forward it to the actual
Job Service to keep it consistent.
"""

import time
import uuid
from typing import List, NamedTuple, Optional

from pydantic import BaseModel, ValidationError
from pymongo import MongoClient
from pymongo.errors import DuplicateKeyError

from .model import ExecStatus, Execution, JobContainer
from .singletons import logger, settings


class NewStatus(NamedTuple):
    """Simplified Wrapper for Execution Status Updates."""

    status: ExecStatus
    exit_code: Optional[int]
    message: Optional[str]


class ExecutionDbClient:
    """Client for Job Execution Status database. The Job's ID is also used for the job-status
    for easier retrieval by job-id. The same client is used for ExecutionRequests and JobContainers,
    all of which are identified by their job_id.
    """

    def __init__(self, mongo_client):
        self.jes_db = mongo_client[settings.database_name]
        self.exec_db = PyMongoClient(self.jes_db["executions"])
        self.cont_db = PyMongoClient(self.jes_db["job-container"])

    def insert_execution(self, obj: Execution):
        """Insert Execution to DB, using Job-ID as DB-ID."""
        self.exec_db.insert(obj.job_id, obj)

    def insert_container(self, obj: JobContainer):
        """Insert Job Container to DB, using Job-ID as DB-ID."""
        self.cont_db.insert(obj.job_id, obj)

    def get_execution(self, job_id: str) -> Optional[Execution]:
        """Get Execution from DB, or None if it does not exist."""
        res = self.exec_db.get(job_id)
        return res and _convert(res, Execution)

    def get_container(self, job_id: str) -> Optional[JobContainer]:
        """Get Job Container from DB, or None if it does not exist."""
        res = self.cont_db.get(job_id)
        return res and _convert(res, JobContainer)

    def get_executions_with_status(self, status: ExecStatus) -> List[Execution]:
        """Find all Executions that are currently SCHEDULED/RUNNING"""
        res = self.exec_db.find({"status": status})
        return [_convert(x, Execution) for x in res]

    def get_old_containers(self, completed_seconds: int) -> List[JobContainer]:
        """Find all JobContainers that completed some time ago (i.e. passed time in seconds)"""
        res = self.exec_db.find({"ended_at": {"$lt": get_time() - completed_seconds}})
        executions = [_convert(x, Execution) for x in res]
        res = self.cont_db.find({"job_id": {"$in": [ex.job_id for ex in executions]}, "is_removed": False})
        return [_convert(x, JobContainer) for x in res]

    def set_removed_flag(self, job_id: str, value: bool = True):
        """Set the is_removed field"""
        self.cont_db.update(job_id, {"is_removed": value})

    def update_execution_status(self, job_id: str, new: NewStatus):
        """Update Execution's status, refresh started/ended times accordingly.
        This method may be called multiple time for the same status transition, i.e. first in JobExecutor.stop_job,
        setting only the message to "stopped", and then again in _await_execution setting the state and exit code.
        """
        cur = self.get_execution(job_id)

        # no not change status if already in another terminal state (but may add/update exit code or message)
        values = {k: v for k, v in new._asdict().items() if v is not None}
        if cur.status not in (ExecStatus.SCHEDULED, ExecStatus.RUNNING):
            del values["status"]

        # add started/ended date based on status transitions
        if cur.status == ExecStatus.SCHEDULED and new.status == ExecStatus.RUNNING:
            values["started_at"] = get_time()
        if cur.status == ExecStatus.RUNNING and new.status != ExecStatus.RUNNING:
            values["ended_at"] = get_time()

        # update in database
        self.exec_db.update(job_id, values)


# Low Level helper methods... if this get an async treatment, those would be converted


class PyMongoClient:
    """Low-Level DB Client for pymongo (synchronous)"""

    def __init__(self, db):
        self.db = db

    def insert(self, id_: str, obj: BaseModel):
        self.db.insert_one({"_id": uuid.UUID(id_), **obj.model_dump()})

    def get(self, id_: str) -> Optional[dict]:
        return self.db.find_one({"_id": uuid.UUID(id_)})

    def find(self, values: dict) -> dict:
        return self.db.find(values)

    def update(self, id_: str, values: dict):
        self.db.update_one({"_id": uuid.UUID(id_)}, {"$set": values})


# Helper Methods


def _convert(obj: dict, cls: type):
    """Remove redundant _id field and convert to given type."""
    if obj is not None:
        del obj["_id"]
        return cls(**obj)


def get_time() -> int:
    """Just a helper for getting time in consistent way"""
    return int(time.time())


def create_db_client():
    """create instance of DB client"""
    mongo_database_url = settings.database_url
    mongo_client = MongoClient(mongo_database_url, uuidRepresentation="standard")
    return ExecutionDbClient(mongo_client)


# Migration


def migrate_db(client: ExecutionDbClient):
    migrate_job_request_status_to_executions(client)
    migrate_new_job_states(client)
    migrate_user_to_org_id(client)


def migrate_job_request_status_to_executions(executions_db: ExecutionDbClient):
    """Migrate old ExecutionRequests and JobStatus to new common Execution format"""
    request_db = executions_db.jes_db["execution-request"]
    status_db = executions_db.jes_db["job-status"]

    for request in request_db.find():
        tmp = {"_id": request["_id"]}
        logger.info("Migrating Request/Status for Job %s to new format", request["_id"])
        status = status_db.res = status_db.find_one(tmp)
        try:
            del request["_id"]
            del status["_id"]
            del status["job_id"]
            executions_db.insert_execution(Execution(**request, **status))
        except (TypeError, ValidationError, DuplicateKeyError) as e:
            logger.error(
                "%s: Request/Status can not be migrated, discarding; Request: %s, Status: %s", e, request, status
            )
        request_db.delete_one(tmp)
        status_db.delete_one(tmp)


def migrate_new_job_states(executions_db: ExecutionDbClient):
    """Migrate old Executions with error_message to new format with exit_code and message."""
    exec_db = executions_db.jes_db["executions"]

    for item in exec_db.find():
        if "error_message" in item:
            exec_db.delete_one(item)
            msg = item.pop("error_message")
            if msg is not None and msg.isdigit():
                item["exit_code"] = int(msg)
            else:
                item["message"] = msg
            if item["status"] in ("COMPLETED", "FAILED"):
                item["status"] = ExecStatus.TERMINATED
            exec_db.insert_one(item)


def migrate_user_to_org_id(executions_db: ExecutionDbClient):
    """Migrate Executions with user_id to ones with organization_id."""
    exec_db = executions_db.jes_db["executions"]

    for item in exec_db.find():
        if "user_id" in item:
            exec_db.delete_one(item)
            item["organization_id"] = item.pop("user_id")
            exec_db.insert_one(item)
