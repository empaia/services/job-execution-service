"""
Test for Authentication features, just some basic requests, not the full API,
with valid, invalid, or missing authentication
"""

import asyncio

import pytest
import requests
from fastapi import HTTPException

from ..auth import check_organization_id, get_sender_auth_client
from ..singletons import settings
from .common import AppTestClient, app_request, get_test_app_id

# SETUP
settings.idp_url = "http://127.0.0.1:10082/auth/realms/EMPAIA"
WBS_ORGANIZATION_ID = ""
WBS_CLIENT_ID = "WBS_CLIENT"
WBS_CLIENT_SECRET = "secret"


def get_client_token(client_id, client_secret):
    auth_client = get_sender_auth_client(client_id, client_secret)
    asyncio.run(auth_client._update_token())  # pylint: disable=W0212
    return auth_client._headers()  # pylint: disable=W0212


auth_headers = get_client_token(WBS_CLIENT_ID, WBS_CLIENT_SECRET)
client = AppTestClient("http://localhost:8000", WBS_ORGANIZATION_ID)


# TESTING AUTH AGAINST SOLUTION STORE SERVICE
# those come first, as working MPS auth is a prerequisite for all other tests


@pytest.mark.skip("MPS Mock does not require auth, so can not be tested in this way")
def test_auth_mps():
    """Test Auth of JES against MPS, independently of any specific Apps.
    If this test fails, the MPS is not available or the authentication changed.
    """
    headers = get_client_token(settings.client_id, settings.client_secret)
    response = requests.get(f"{settings.mps_url}/api/v0/app/list", headers=headers, timeout=5)
    assert response.status_code == 200


def test_jes_test_app():
    """Check whether the JES Test App exists, and which ID it uses.
    If this test fails, the Test App was re-deployed and the ID used here needs adapting.
    """
    assert get_test_app_id() is not None, "JES Test App not found"


# TESTING AUTH AGAINST JOB EXECUTION SERVICE ITSELF


def test_auth_valid():
    """Execute Job and get status with valid Access Token."""
    headers = auth_headers

    request = app_request()
    response = client.post_execution(request, **headers)
    assert response.status_code == 200

    response = client.get_execution(request["job_id"], **headers)
    assert response.status_code == 200

    response = client.stop_execution(request["job_id"], **headers)
    assert response.status_code == 200


def test_auth_invalid():
    """Try to execute job with invalid Access Token."""
    headers = auth_headers.copy()
    headers["Authorization"] = headers["Authorization"].replace("G", "Q")

    request = app_request()
    response = client.post_execution(request, **headers)
    assert response.status_code == 401  # Unauthorized

    response = client.get_execution(request["job_id"], **headers)
    assert response.status_code == 401

    response = client.stop_execution(request["job_id"], **headers)
    assert response.status_code == 401


def test_auth_missing():
    """Try to execute job with missing Access Token"""
    request = app_request()
    response = client.post_execution(request)
    assert response.status_code == 403  # Forbidden

    response = client.get_execution(request["job_id"])
    assert response.status_code == 403

    response = client.stop_execution(request["job_id"])
    assert response.status_code == 403


# CAN NOT EASILY GENERATE A TOKEN WITH ORGANIZATION ID, SO ABOVE AUTH TESTS ARE WITHOUT ORG TOKEN
# BELOW TESTS TEST THE CHECK-ORGANIZATION-ID FUNCTION IN ISOLATION


def test_org_id_valid():
    token = {"organizations": {"some-organization": {}}}
    check_organization_id(token, True, "", "some-organization")


def test_org_id_invalid():
    token = {"organizations": {"some-organization": {}}}
    with pytest.raises(HTTPException) as e:
        check_organization_id(token, True, "", "some-other-organization")
    assert e.value.status_code == 403
    assert "does not match organizations in token" in e.value.detail.lower()


def test_org_id_valid_onprem():
    token = {"organizations": {"some-organization": {}}}
    check_organization_id(token, False, "some-organization", "some-organization")


def test_org_id_invalid_onprem():
    token = {"organizations": {"some-organization": {}}}
    with pytest.raises(HTTPException) as e:
        check_organization_id(token, False, "some-other_organization", "some-organization")
    assert e.value.status_code == 403
    assert "does not match jes's own" in e.value.detail.lower()


def test_org_id_missing():
    token = {"organizations": {"some-organization": {}}}
    with pytest.raises(HTTPException) as e:
        check_organization_id(token, True, "", "")
    assert e.value.status_code == 403
    assert "header not found" in e.value.detail.lower()


def test_org_id_not_required():
    token = {}
    check_organization_id(token, True, "", "does-not-matter")
