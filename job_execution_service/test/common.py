import functools
import time
import uuid

import requests


class AppTestClient:
    def __init__(self, base_url, organization_id):
        self.base_url = base_url
        self.organization_id = organization_id

    def alive(self):
        return requests.get(f"{self.base_url}/alive", timeout=10)

    def post_execution(self, request, **additional_headers):
        header = {"organization-id": self.organization_id, **additional_headers}
        return requests.post(f"{self.base_url}/v1/executions", json=request, headers=header, timeout=10)

    def get_execution(self, job_id, **additional_headers):
        header = {"organization-id": self.organization_id, **additional_headers}
        return requests.get(f"{self.base_url}/v1/executions/{job_id}", headers=header, timeout=10)

    def stop_execution(self, job_id, **additional_headers):
        header = {"organization-id": self.organization_id, **additional_headers}
        return requests.put(f"{self.base_url}/v1/executions/{job_id}/stop", headers=header, timeout=10)

    def wait_status(self, job_id: str, timeout: int, expected: str, not_expected=tuple()):
        """Repeatedly poll execution status until given status or timeout is reached"""
        status = None
        for _ in range(timeout):
            time.sleep(1)
            response = self.get_execution(job_id)
            execution = response.json()
            status = execution["status"]
            assert status not in not_expected
            if status == expected:
                break
            if status in ("ERROR", "TERMINATED", "STOPPED", "TIMEOUT"):
                assert False, f"Unexpected terminal status: {status} (message: {execution['message']})"
        else:
            assert False, f"Did not reach status {expected} in time; last status was {status}"


# HELPER FUNCTIONS


@functools.lru_cache(None)
def get_test_app_id():
    # since app is fixed in apps_v1.json, can hard code ID here
    # for future, maybe search via /v1/customer/portal-apps/query
    return "b10648a7-340d-43fc-a2d9-4d91cc86f33f"


def app_request(running_time=0, fail=False, timeout=-1, app_id=None):
    """Create a simple ExecutionRequest for testing. The parameters can be used to "encode" information
    into the request itself that will be used by the test-app for showing different behaviours.

    This creates a Job Request as follows, that can also be copied to the Web-UI for convenience for
    manual testing; don't forget to slightly adapt the `job_id` for each execution, though.

        {
           "app_id": "03f84602-bed7-4f2a-889c-e99473a7f0fd",
           "job_id": "cf0c35e9-7bee-4fc9-a5f2-9ac8e7fbbe30",
           "access_token": "does-not-matter",
           "app_service_url": "3",
           "timeout": -1
         }
    """
    return {
        "app_id": app_id or get_test_app_id(),
        "job_id": str(uuid.uuid4()),
        "access_token": "does-not-matter",
        "app_service_url": f"{running_time}" if not fail else f"{running_time} and then fail",
        "timeout": timeout,
    }


def get_error(response):
    return response.json()["detail"]["cause"]


def get_error_api(response):
    return response.json()["detail"][0]["msg"]


def check_time(timestamp, delta=0.0):
    """the delta is pretty big here; it basically only checks that the timestamp is in the right ballpark,
    not millisecond precision; that would be difficult with test timing"""
    return abs(time.time() - delta - timestamp) <= 2.0
