"""
Test the Main API of the Job Execution Service. The tests in this module do not use authentication
but just test how the API responds to different kinds of valid in invalid requests, that Job Statuses
are set correctly, etc. This module now also tests the actual Docker Runner (previously this was in
a separate module) implementation with actual Docker Images.

All tests in this module are run using a simple Test App that just starts, runs for a few
seconds, and then either exits normally or fails depending on some of the parameters given
as environment variables. It does not require any interaction with the App Service.
"""

import time
import uuid

import pytest
import requests.exceptions

from ..model import ExecStatus, Execution
from ..singletons import settings
from .common import AppTestClient, app_request, check_time, get_error, get_error_api

# pylint: disable=E1135

# SETUP

client = AppTestClient("http://localhost:8000", organization_id="some random organization")


# TEST CASES: Basic API tests (how it behaves on different edge cases)


def test_alive():
    """Test whether the JES is there at all."""
    try:
        response = client.alive()
        assert response.status_code == 200
        return
    except requests.exceptions.ConnectionError:
        pass
    # having this assert in the `except` block prints the entire stacktrace, which is confusing
    assert False, "JES not found; did you forget to start it before testing?"


def test_check_auth_setting():
    """this does not test the JES but how this test is called and prints a helpful notification"""
    response = client.get_execution(uuid.uuid4())
    assert response.status_code != 403, "Make sure to use JES_ENABLE_RECEIVER_AUTH=False for this test suite!"


def test_execute_invalid_request():
    """test execute-job request with missing attributes"""
    # those are checked by FastAPI directly, just trying for one missing attribute to see the error
    request = {k: v for k, v in app_request().items() if k != "access_token"}
    response = client.post_execution(request)
    assert response.status_code == 422
    assert "Field required" in get_error_api(response)


def test_execute_invalid_id():
    """try to execute an execution-request with an invalid ID"""
    request = {**app_request(), "job_id": "x" * 5}
    response = client.post_execution(request)
    assert response.status_code == 400
    assert "badly formed" in get_error(response)


def test_execute_success():
    """test valid execute-job request, then get status of the same job"""
    request = app_request()
    response = client.post_execution(request)
    assert response.status_code == 200
    response = client.get_execution(request["job_id"])
    assert response.status_code == 200


def test_execute_again():
    """try to execute the same job (same ID) again, which should not be allowed"""
    request = app_request()
    response = client.post_execution(request)
    assert response.status_code == 200
    response = client.post_execution(request)
    assert response.status_code == 409
    assert "already been executed" in get_error(response)


def test_execute_no_organizationid():
    """try to execute a job without passing an organization id header"""
    request = app_request()
    response = client.post_execution(request, **{"organization-id": None})
    assert response.status_code == 422


def test_execute_app_not_found():
    """existence of App/Docker image is checked immediately before Job is scheduled or started"""
    request = app_request(app_id=str(uuid.uuid4()))  # random non-existing App ID
    response = client.post_execution(request)
    assert response.status_code == 404
    assert "not found" in get_error(response)


def test_get_status_unknown_id():
    """try to get job status for unknown job"""
    response = client.get_execution(uuid.uuid4())
    assert response.status_code == 404
    assert "not found" in get_error(response)


def test_get_status_invalid_id():
    """try to get job status with invalid job ID"""
    response = client.get_execution("x" * 5)
    assert response.status_code == 400
    assert "badly formed" in get_error(response)


def test_get_status_mismatched_organizationid():
    """execute job, then try to access it with a different organization-id header"""
    request = app_request()
    client.post_execution(request)
    response = client.get_execution(request["job_id"], **{"organization-id": "some-other-random-organization-id"})
    assert response.status_code == 412


def test_stop_unknown_id():
    """like for status, test behaviour on unknown job id"""
    response = client.stop_execution(uuid.uuid4())
    assert response.status_code == 404


def test_stop_mismatched_organization():
    """like for status, test behaviour for mismatched organization id"""
    request = app_request()
    client.post_execution(request)
    response = client.stop_execution(request["job_id"], **{"organization-id": "some-other-random-organization-id"})
    assert response.status_code == 412


# TEST CASES testing the actual execution of Apps and the stages they go through


def test_execute_job_running():
    """test executing a dummy job that will run for a few seconds, check status of the job"""
    # clear up built-up jobs after very fast API tests not waiting for results
    time.sleep(5)

    request = app_request(running_time=2)
    client.post_execution(request)
    client.wait_status(request["job_id"], 5, ExecStatus.RUNNING)

    response = client.get_execution(request["job_id"])
    status = Execution(**response.json())
    assert status.status == ExecStatus.RUNNING
    assert status.started_at is not None
    assert status.ended_at is None


def test_execute_job_finish():
    """test executing a dummy job that will finish quickly, check status of the job"""
    request = app_request(running_time=0)
    client.post_execution(request)
    client.wait_status(request["job_id"], 5, ExecStatus.TERMINATED)

    response = client.get_execution(request["job_id"])
    status = Execution(**response.json())
    assert status.status == ExecStatus.TERMINATED
    assert status.ended_at is not None


def test_execute_job_fail():
    """test executing a dummy job that will fail, check status of the job"""
    request = app_request(running_time=0, fail=True)
    client.post_execution(request)
    client.wait_status(request["job_id"], 4, ExecStatus.TERMINATED)

    response = client.get_execution(request["job_id"])
    status = Execution(**response.json())
    assert status.status == ExecStatus.TERMINATED
    assert status.exit_code != 0
    assert status.ended_at is not None


def test_execute_job_timeout():
    """test executing a dummy job that will fail, check status of the job"""
    request = app_request(running_time=3, timeout=1)
    client.post_execution(request)
    client.wait_status(request["job_id"], 5, ExecStatus.TIMEOUT, (ExecStatus.TERMINATED,))

    response = client.get_execution(request["job_id"])
    status = Execution(**response.json())
    assert status.status == ExecStatus.TIMEOUT
    assert "timeout" in status.message
    assert status.ended_at is not None


# can currently not be tested; no way to provoke error without mocking
@pytest.mark.skip
def test_execute_job_error():
    """try to execute a job with an actual error happening in the execute-job-function"""
    request = app_request(running_time=0)
    client.post_execution(request)
    client.wait_status(request["job_id"], 5, ExecStatus.ERROR, (ExecStatus.TERMINATED,))

    response = client.get_execution(request["job_id"])
    status = Execution(**response.json())
    assert status.status == ExecStatus.ERROR
    assert "error" in status.message
    assert status.ended_at is not None


def test_execute_started_ended():
    """execute a job, wait for it to be completed, then check the started-at and ended-at times"""
    request = app_request(running_time=1)
    client.post_execution(request)
    client.wait_status(request["job_id"], 5, ExecStatus.TERMINATED)

    response = client.get_execution(request["job_id"])
    status = Execution(**response.json())
    assert status.status == ExecStatus.TERMINATED
    assert check_time(status.started_at, 3)
    assert check_time(status.ended_at, 2)


def test_execute_get_attributes():
    """test whether all attributes form the request are in the full execution, except access token"""
    request = app_request(running_time=0)
    response = client.post_execution(request)
    res = response.json()
    assert all(res[x] == request[x] for x in request)
    response = client.get_execution(request["job_id"])
    res = response.json()
    assert all(res[x] == request[x] if x != "access_token" else res[x] != request[x] for x in request)


# TEST CASES about Stopping/Cancelling Executions


def test_stop():
    """post a job, wait until it started, then stop it"""
    request = app_request(running_time=5)
    client.post_execution(request)
    client.wait_status(request["job_id"], 5, ExecStatus.RUNNING)

    response = client.stop_execution(request["job_id"])
    assert response.text == "true"
    client.wait_status(request["job_id"], 5, ExecStatus.STOPPED, (ExecStatus.TERMINATED,))

    response = client.get_execution(request["job_id"])
    status = Execution(**response.json())
    assert status.status == ExecStatus.STOPPED
    assert "stopped" in status.message
    assert status.exit_code == 137


def test_stop_already_stopped():
    """post a job, wait for completion, then try to stop it"""
    request = app_request(running_time=0)
    client.post_execution(request)
    client.wait_status(request["job_id"], 3, ExecStatus.TERMINATED)

    response = client.stop_execution(request["job_id"])
    assert response.text == "false"


@pytest.mark.skip  # using retired max_number_of_jobs, skipping for now
def test_stop_not_yet_started():
    """post a job, then abort it before it had a chance to start"""
    # create jobs to fill job queue
    for _ in range(settings.max_number_of_jobs + 1):
        client.post_execution(app_request(running_time=3))
    # now schedule the job we want to test
    request = app_request(running_time=1)
    client.post_execution(request)
    # make sure it did not start yet
    response = client.get_execution(request["job_id"])
    status = Execution(**response.json())
    assert status.status == ExecStatus.SCHEDULED

    # abort and check status
    response = client.stop_execution(request["job_id"])
    assert response.text == "true"
    client.wait_status(
        request["job_id"], 2, ExecStatus.STOPPED, (ExecStatus.SCHEDULED, ExecStatus.RUNNING, ExecStatus.TERMINATED)
    )
    response = client.get_execution(request["job_id"])
    status = Execution(**response.json())
    assert "stopped" in status.message
    assert status.status == ExecStatus.STOPPED


# TEST CASES involving more complex setups, with multiple jobs, etc.
# some of those tests only apply to some variants of the JES, and others are very dependent on
# timing, and are thus skipped by default


# @pytest.mark.skipif(not bool(settings.docker_gpu_driver), reason="GPU not supported by this JES")
@pytest.mark.skip
def test_gpu_usage():
    """Test GPU usage by app, provided that it is enabled in JES settings. This will require the
    JES Test App to also have a "GPU mode"
    """


# the Event-Based Runner _does_ check whether Containers can still be found before waiting for
# the Container after a restart, which makes sense, but not _again_ after waiting, which would
# not make very much sense, but thus this test would fail (and the actual restart can not be tested)
# @pytest.mark.skipif(settings.job_runner == "EVENTED", reason="Not tested for EVENTED runner")
# sneakily changing the container is no longer possible with the streamlined DB client
@pytest.mark.skip
def test_execute_job_container_not_found():
    """Start a Job, but then for whatever reason to get the Docker Container. This should never
    happen, but might if there is some sort of DB hickup or if the JES is re-started with a
    different Docker host.
    """
    request = app_request(running_time=3)
    client.post_execution(request).raise_for_status()

    # once the job is running, sneakily change its container ID in the database...
    client.wait_status(request["job_id"], 3, ExecStatus.RUNNING)

    # get_containers_client().update(request["job_id"], {"container_id": str(uuid.uuid4())})

    # now the container can no longer be found (simulating JES restart with different Docker host)
    client.wait_status(request["job_id"], 2, ExecStatus.ERROR, (ExecStatus.TERMINATED,))


@pytest.mark.skip
def test_execute_multiple_jobs():
    """Execute Multiple Jobs in parallel and see if they succeed, and that not more than allowed run in parallel"""
    time.sleep(10)
    # start a bunch of Jobs
    allowed = settings.max_number_of_jobs
    request_list = [app_request(running_time=5) for _ in range(allowed * 2)]
    for request in request_list:
        client.post_execution(request).raise_for_status()

    # at any time, only X jobs should be running in parallel (allow for a little leeway due to time for startup loop)
    time.sleep(8)
    statuses = [client.get_execution(r["job_id"]).json()["status"] for r in request_list]
    assert allowed - 1 <= statuses.count("COMPLETED") <= allowed + 1  # one batch completed...
    assert allowed - 1 <= statuses.count("RUNNING") <= allowed  # one batch in progress

    # after a few more seconds all jobs should be completed
    time.sleep(8)
    statuses = [client.get_execution(r["job_id"]).json()["status"] for r in request_list]
    assert statuses.count("COMPLETED") == allowed * 2


@pytest.mark.skip
def test_execute_concurrent_jobs():
    """Execute three concurrent jobs with different timings and see if last, shorter one
    finishes before first, long ones
    """
    time.sleep(10)
    request_list = []
    for i in [6, 3, 0]:
        request = app_request(running_time=i)
        request_list.append(request)
        client.post_execution(request).raise_for_status()

    time.sleep(12)

    response_list = []
    for request in request_list:
        response = client.get_execution(request["job_id"])
        status = Execution(**response.json())
        response_list.append(status.ended_at)

    assert response_list[0] > response_list[1] > response_list[2]


def test_openapi():
    """Test whether openapi.json is available"""
    response = requests.get("http://localhost:8000/openapi.json", timeout=10)
    assert response.status_code == 200
    assert response.json()["info"]["title"] == "Job Execution Service"
