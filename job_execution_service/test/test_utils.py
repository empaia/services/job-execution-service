from ..container_clients.kubernetes import convert_image_name_to_labels
from ..jobexecutor import parse_worker_spec

REPO = "repo"
NAME = "name"
TAG = "tag"
HASH = "hash"


def test_image_name_to_labels():
    """test if the function for converting an image name to pod labels"""
    image_name = f"{REPO}/{NAME}:{TAG}@{HASH}"

    returned_name, returned_tag = convert_image_name_to_labels(image_name)
    assert NAME == returned_name
    assert TAG == returned_tag


def test_image_name_to_labels_without_hash():
    """test if the function for converting an image name without hash to pod labels"""
    image_name_without_hash = f"{REPO}/{NAME}:{TAG}"
    returned_name, returned_tag = convert_image_name_to_labels(image_name_without_hash)
    assert NAME == returned_name
    assert TAG == returned_tag


def test_image_name_to_labels_without_repo():
    """test if the function for converting an image name without repo to pod labels"""
    image_name_without_repo = f"{NAME}:{TAG}@{HASH}"
    returned_name, returned_tag = convert_image_name_to_labels(image_name_without_repo)
    assert NAME == returned_name
    assert TAG == returned_tag


def test_image_name_to_labels_without_repo_and_hash():
    """test if the function for converting an image name without repo and hash to pod labels"""
    image_name_without_repo_and_hash = f"{NAME}:{TAG}"
    returned_name, returned_tag = convert_image_name_to_labels(image_name_without_repo_and_hash)
    assert NAME == returned_name
    assert TAG == returned_tag


def test_image_name_to_labels_without_tag_and_hash():
    """test if the function for converting an image name without tag and has to pod labels"""
    image_name_without_tag_hash = f"{REPO}/{NAME}"
    returned_name, returned_tag = convert_image_name_to_labels(image_name_without_tag_hash)
    assert NAME == returned_name
    assert "" == returned_tag


def test_image_name_to_labels_with_forwardslash_before():
    """test if the function for converting an image name with extra forwardslash before the actual name to pod labels"""
    image_name_with_extra_forwardslash_before = f"{REPO}/extra-slash/{NAME}:{TAG}@{HASH}"
    returned_name, returned_tag = convert_image_name_to_labels(image_name_with_extra_forwardslash_before)
    assert NAME == returned_name
    assert TAG == returned_tag


def test_image_name_to_labels_with_forwardslash_after():
    """test if the function for converting an image name with extra forwardslash after the actual name to pod labels"""
    image_name_with_extra_forwardslash_after = f"{REPO}/{NAME}/extra-slash:{TAG}@{HASH}"
    returned_name, returned_tag = convert_image_name_to_labels(image_name_with_extra_forwardslash_after)
    assert "extra-slash" == returned_name
    assert TAG == returned_tag


def test_image_name_to_labels_with_special_characters():
    """test if the function can cope with special characters"""
    image_name_with_extra_forwardslash_after = f"https://{REPO}?/!{NAME}%=?:${TAG}@{HASH}458##§"
    returned_name, returned_tag = convert_image_name_to_labels(image_name_with_extra_forwardslash_after)
    assert "_" + NAME + "___" == returned_name
    assert "_" + TAG == returned_tag


def test_parse_workers():
    spec = "tcp://host1:2345;1,2,3|tcp://host2:2345;|;|tcp://host3:2345;x5||"
    workers = parse_worker_spec(spec)
    assert workers[0] == ("tcp://host1:2345", True, "1,2,3")  # GPU with device IDs
    assert workers[1] == ("tcp://host2:2345", True, "")  # GPU, all devices
    assert workers[2] == ("", True, "")  # default host, GPU, all devices
    assert workers[3] == ("tcp://host3:2345", False, "")  # no GPU, first of five
    assert workers[7] == ("tcp://host3:2345", False, "")  # no GPU, last of five
    assert len(workers) == 8  # empty groups and trailing "|" are ignored
