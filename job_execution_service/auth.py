"""
For handling OAuth authentication, taken and adapted from ID Mapper Service.
"""

from fastapi import Depends, HTTPException, Request
from fastapi.openapi.models import OAuthFlowClientCredentials, OAuthFlows
from fastapi.security import OAuth2
from pydantic import ValidationError

from .empaia_receiver_auth import Auth
from .empaia_sender_auth import AioHttpClient, AuthSettings
from .singletons import logger, settings


def get_receiver_auth():
    return JESAuth(settings.enable_receiver_auth)


def get_sender_auth_client(client_id=None, client_secret=None):
    auth_settings = None
    try:
        auth_settings = AuthSettings(
            idp_url=settings.idp_url,
            client_id=client_id or settings.client_id,
            client_secret=client_secret or settings.client_secret,
        )
    except ValidationError as e:
        logger.warning("Auth not configured: %s", e)
    return AioHttpClient(logger=logger, auth_settings=auth_settings, enable_custom_mode=False)


def _dummy():
    pass


def check_organization_id(decoded_token: dict, is_compute_provider: bool, org_id_jes: str, org_id_header: str):
    """check validity of organization id by checking that
    - organization id header is present
    - it matches one of the organizations in the JWT
    - it matches JES's own organization if not compute provider
    """
    if "organizations" not in decoded_token:
        return
    if not org_id_header:
        raise HTTPException(status_code=403, detail="Organization-ID header not found")
    if org_id_header not in decoded_token.get("organizations", []):
        raise HTTPException(status_code=403, detail="Organization-ID header does not match Organizations in Token")
    if not is_compute_provider and org_id_header != org_id_jes:
        raise HTTPException(status_code=403, detail="Organization-ID header does not match JES's own Organization")


def make_oauth2_wrapper(auth: Auth, openapi_token_url: str):
    oauth2_scheme = OAuth2(flows=OAuthFlows(clientCredentials=OAuthFlowClientCredentials(tokenUrl=openapi_token_url)))

    def oauth2_wrapper(request: Request, token=Depends(oauth2_scheme)):
        decoded_token = auth.decode_token(token)
        check_organization_id(
            decoded_token,
            settings.is_compute_provider,
            settings.own_organization_id,
            request.headers.get("organization-id"),
        )
        return decoded_token

    return oauth2_wrapper


class JESAuth:
    """Providing all the 'depends' and 'hooks' for the Job Execution Service,
    either with or without actually requiring authentication.
    """

    def __init__(self, use_auth: bool):
        if use_auth:
            auth = Auth(
                idp_url=settings.idp_url.rstrip("/"),
                refresh_interval=settings.refresh_interval,
                audience=settings.audience,
                rewrite_url_in_wellknown=settings.rewrite_url_in_wellknown,
                logger=logger,
            )
            self.oauth2_wrapper = make_oauth2_wrapper(auth=auth, openapi_token_url=settings.openapi_token_url)
        else:
            self.oauth2_wrapper = _dummy

    def execute_job_depends(self):
        return Depends(self.oauth2_wrapper)

    def execute_job_hook(self, payload, request):
        pass

    def get_status_depends(self):
        return Depends(self.oauth2_wrapper)

    def get_status_hook(self, payload, job_id):
        pass

    def stop_job_depends(self):
        return Depends(self.oauth2_wrapper)

    def stop_job_hook(self, payload, job_id):
        pass
