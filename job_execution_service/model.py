"""
This module defines the models used by the job-execution-service. The Job itself
is not among them, but just a shorter job-execution and a separate job-container that
is stored in a database to be synchronized with the actual Job's status.
"""

from enum import Enum
from typing import Optional

from pydantic import Field

from .models.v3.commons import Id, RestrictedBaseModel, Timestamp
from .models.v3.job import JobMode

# MODELS FOR JOB EXECUTION SERVICE


class ExecStatus(str, Enum):
    """Current status of a Job Execution"""

    SCHEDULED = "SCHEDULED"  # scheduled for execution
    RUNNING = "RUNNING"  # currently running
    TERMINATED = "TERMINATED"  # completed by itself, with any return code
    STOPPED = "STOPPED"  # stopped by user
    TIMEOUT = "TIMEOUT"  # process killed after Request timeout
    ERROR = "ERROR"  # indicating error with job executor, i.e. outside of app


class AppType(str, Enum):
    GPU = "GPU"  # app requires GPU
    CPU = "CPU"  # app does not require GPU
    PROXY = "PROXY"  # app is mostly a proxy for some external service


class AppRequirements(RestrictedBaseModel):
    """Class encapsulating different requirements for an app. For now, only a 'type' of app,
    later also numeric requirements for CPU, Memory, VRAM, etc."""

    type: AppType = Field(
        examples=[AppType.GPU],
        description="The type of the app, e.g. whether it requires GPU, or is merely a proxy",
    )


class PostExecution(RestrictedBaseModel):
    """Class encapsulating parameters for the Job Execution Service."""

    app_id: Id = Field(
        examples=["b10648a7-340d-43fc-a2d9-4d91cc86f33f"],
        description="The ID of the App to execute, needed to get the App Image from the Solution Store",
    )

    job_id: Id = Field(
        examples=["c430265d-ba50-4c17-98f8-85028b6a1ec7"],
        description="The ID of the job to execute, passed to the App and further to the App Service",
    )

    access_token: str = Field(
        examples=[
            "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiI3NTYzMDAxOC03ZjhmLTQ3YjgtODZmNC0wMTliODhjNjZhMTEiLCJle"
            "HAiOjE2MjQzNTkwNTZ9.CnT0NYwVzyNl05Jp0z4W-qDqKjolQHZxmT9i7SYyBYG6D-5K7jLxm3l4lBLp30rnjYOiZm0TtvskK1lYDh"
            "gKNyXnEhB_O7f6DQuTd9tn8yv8XnK19pj6g8nubFfBho9lYhComb6a3XX3vqLK5pnaXuhC9tFdzsnLkQPoIi2DZ8E"
        ],
        description="Access-Token used for accessing the Job data; passed to App by JES. and further to App-Service "
        "to validate authenticity of the Job; decodes as {'sub': <job-id>, 'exp': <time>}.",
    )

    app_service_url: str = Field(
        ...,
        examples=["https://www.example.com/app-service"],
        description="Where the App can reach the App Service",
    )

    timeout: int = Field(
        -1,
        examples=[60],
        description="Timeout (in seconds) when to automatically kill the job, optional, negative means 'no timeout'",
    )

    mode: JobMode = Field(
        JobMode.STANDALONE,
        examples=[JobMode.STANDALONE],
        description="The mode of the job corresponding to a mode in the EAD",
    )


class Execution(PostExecution):
    """Wrapper for a status and an optional error message."""

    status: ExecStatus = Field(
        examples=[ExecStatus.TIMEOUT],
        description="The actual Status of the Job",
    )

    exit_code: Optional[int] = Field(
        default=None,
        examples=["137"],
        description="Information on abnormal status, e.g. 'timeout', 'stopped by user', or internal error message",
    )

    message: Optional[str] = Field(
        default=None,
        examples=["timeout"],
        description="Information on abnormal status, e.g. timeout, cancelled, or internal error message",
    )

    created_at: Optional[Timestamp] = Field(
        default=None,
        examples=[1623349180],
        description="Time when execution of the job was scheduled",
    )

    started_at: Optional[Timestamp] = Field(
        default=None,
        examples=[1623349185],
        description="Time when execution of the job was started",
    )

    ended_at: Optional[Timestamp] = Field(
        default=None,
        examples=[1623369200],
        description="Time when the job was completed or when it failed",
    )

    organization_id: str = Field(
        examples=["137396e9-8374-4b75-9cb5-31988bc58171"],
        description="The Organization ID that was used for creating the Job",
    )


class JobContainer(RestrictedBaseModel):
    """For storing additional information about the container when the Job is running, which belongs into
    neither the Execution Request nor the Job Status. Currently, this is just the Container ID, but more
    might be added later, e.g. how the container was executed (Docker, Kubernetes) and possibly additional
    information depending on the mode.
    """

    job_id: Id = Field(
        examples=["fe9f3436-b6ce-4881-acea-1048543f0b80"],
        description="The ID of the job",
    )

    container_id: str = Field(
        examples=["d6227387799c"],
        description="Docker Container ID or similar while the Job is running",
    )

    is_removed: bool = Field(
        default=False,
        examples=[True],
        description="A flag which determines if a container is removed from disk",
    )
