from typing import List, Optional

from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    """Parameters to be loaded from Environment Variables in .env file"""

    # API
    disable_openapi: bool = False
    root_path: Optional[str] = None

    # Database
    database_url: str
    database_name: str = "job-execution-service-db"

    # Job Runner Configuration
    time_to_clean_up_jobs_after_completion: int = -1  # in seconds; -1 means no clean up

    # worker specifications for different queues
    worker_standalone_gpu: str
    worker_standalone_cpu: str
    worker_standalone_proxy: str
    worker_preprocessing_gpu: str
    worker_preprocessing_cpu: str
    worker_preprocessing_proxy: str

    # Docker
    docker_host: str = ""  # mapped to regular env variable docker_host
    docker_network: Optional[str] = None  # network to connect apps if using DockerClient
    docker_extra_hosts: Optional[str] = None  # extra_hosts name resolution mappings separated by ,
    docker_extra_env_vars: List[str] = []  # additional env vars to pass to App, key=value
    docker_always_pull: bool = True  # always try to re-pull the image from registry
    docker_gpu_driver: str = ""  # driver for requesting GPU device, e.g. "nvidia"
    docker_gpu_count: int = 5  # max number of GPUs usable by a Job if not explicit device-ids are set
    docker_reconnect_sec: int = 60  # reconnect after X second to recover from losing connection to host

    # Kubernetes
    kubernetes_namespace: Optional[str] = None
    kubernetes_config_path: Optional[str] = None
    kubernetes_gpu_driver: str = ""  # driver for requesting GPU device, e.g. "nvidia.com/gpu"
    kubernetes_always_pull: bool = True  # always try to re-pull the image from registry
    kubernetes_node_label_value: Optional[str] = None  # value of node label for exclusive scheduling, key is "empaia"

    # Authentication
    idp_url: str
    openapi_token_url: str
    client_id: str
    client_secret: str
    audience: str
    enable_receiver_auth: bool = False
    rewrite_url_in_wellknown: Optional[str] = None
    refresh_interval: int = 300  # seconds

    # Marketplace Service Integration & Docker Registries
    mps_url: str
    registry_names: str = ""
    registry_logins: str = ""
    registry_pwds: str = ""
    registry_separator: str = ";"
    is_compute_provider: bool = False
    own_organization_id: Optional[str] = None
    app_requirements_file: str = "/artifacts/app_requirements.json"

    # Parameters for the Promtail/Loki/Grafana logging service
    topic_label: str = "empaia"
    log_label: str = "jes"

    model_config = SettingsConfigDict(env_prefix="JES_", extra="forbid")
